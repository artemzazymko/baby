<?php
return [
    'Compare' => 'Сравнение',
    'Store' => 'Магазин',
    'Compare products module' => 'Модуль сравнения товаров',
    'onePlus team' => 'Команда OnePlus',
    'Compare product' => 'Сравнения товаров',
    'Compare table' => 'Таблица сравненния',
    'Success added!' => 'Добавлено',
    'Success removed!' => 'Удаленно',
];