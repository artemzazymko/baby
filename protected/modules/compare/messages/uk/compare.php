<?php
return [
    'Compare' => 'Порівняння',
    'Store' => 'Магазин',
    'Compare products module' => 'Модуль порівняння товарів',
    'onePlus team' => 'Команда OnePlus',
    'Compare product' => 'Порівняння товарів',
    'Compare table' => 'Таблиця порівняння',
    'Success added!' => 'Додано',
    'Success removed!' => 'Видалено',
];