<?php

/**
 * Class CompareControl
 */
class CompareControl extends \yupe\widgets\YWidget
{
    /**
     * @var
     */
    public $product;

    /**
     * @var
     */
    public $compare;

    /**
     * @var string
     */
    public $view = 'compare';

    /**
     *
     */
    public function init()
    {
        Yii::app()->getClientScript()->registerScriptFile(
            Yii::app()->getAssetManager()->publish(
                Yii::getPathOfAlias('application.modules.compare.view.web') . '/compare.js'
            ),
            CClientScript::POS_END
        );

        $this->compare = Yii::app()->getComponent('compare');

        parent::init();
    }

    /**
     * @throws CException
     */
    public function run()
    {
        $this->render($this->view, ['product' => $this->product, 'compare' => $this->compare]);
    }
}