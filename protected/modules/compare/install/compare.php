<?php

return [
    'module'    => [
        'class' => 'application.modules.compare.CompareModule',
    ],
    'import' => [
        'application.modules.compare.components.CompareService',
        'application.modules.compare.listeners.CompareTemplateListener',
        'application.modules.compare.events.*'
    ],
    'component' => [
        'compare' => [
            'class' => 'application.modules.compare.components.CompareService'
        ],
        'eventManager'   => [
            'class'  => 'yupe\components\EventManager',
            'events' => [
                'template.head.start' => [
                    ['CompareTemplateListener', 'js']
                ]
            ]
        ]
    ],
    'rules' => [
        '/compare/add' => '/compare/default/add',
        '/compare/remove' => '/compare/default/remove',
        '/compare' => '/compare/default/index',
    ],
];