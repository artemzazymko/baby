/*добавление/удаление товаров в избранное*/
$(document).ready(function () {
    $(document).on('click', '.yupe-store-compare-add', function (event) {
        event.preventDefault();
        var $this = $(this);
        var product = parseInt($this.data('id'));
        var data = {'id': product};
        data[yupeTokenName] = yupeToken;
        $.post(yupeStoreAddCompareUrl, data, function (data) {
            if (data.result) {
                $('#yupe-store-compare-total').html(data.count);
                $this.removeClass('yupe-store-compare-add')
                    .addClass('yupe-store-compare-remove').addClass('text-error');
                $this.children('i').addClass('added');
            }
            showNotify($this, data.result ? 'success' : 'danger', data.data);
        }, 'json');
    });

    $(document).on('click', '.yupe-store-compare-remove', function (event) {
        event.preventDefault();
        var $this = $(this);
        var product = parseInt($this.data('id'));
        var data = {'id': product};
        data[yupeTokenName] = yupeToken;
        $.post(yupeStoreRemoveCompareUrl, data, function (data) {
            if (data.result) {
                $('#yupe-store-compare-total').html(data.count);
                $this.removeClass('yupe-store-compare-remove')
                    .removeClass('text-error').addClass('yupe-store-compare-add');
                $this.children('i').removeClass('added');
            }
            showNotify($this, data.result ? 'success' : 'danger', data.data);
        }, 'json');
    });
});