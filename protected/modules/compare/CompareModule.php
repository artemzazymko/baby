<?php

/**
 * Class CompareModule
 */
class CompareModule extends yupe\components\WebModule
{
    /**
     *
     */
    const VERSION = '1.0';

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setImport(array(
            'compare.components.*',
        ));
    }

    /**
     * @return bool
     */
    public function getIsShowInAdminMenu()
    {
        return false;
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return ['store'];
    }

    /**
     * @return bool
     */
    public function getAdminPageLink()
    {
        return false;
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        return [
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => 'Список задач',
                'url'   => ['/compare/compare/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => 'Создать задачу',
                'url'   => ['/compare/compare/create']
            ],
        ];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('CompareModule.compare', 'Compare');
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('CompareModule.compare', 'Store');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('CompareModule.compare', 'Compare products module');
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        return self::VERSION;
    }

    /**
     * @return string
     */
    public function getAuthor()
    {
        return Yii::t('CompareModule.compare', 'onePlus team');
    }

    /**
     * @return string
     */
    public function getAuthorEmail()
    {
        return Yii::t('CompareModule.compare', 'artem.zazymko@gmail.com');
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return Yii::t('CompareModule.compare', 'http://yupe.ru');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa-balance-scale';
    }
}
