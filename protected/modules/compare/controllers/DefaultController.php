<?php

use yupe\components\controllers\FrontController;

/**
 * Class DefaultController
 */
class DefaultController extends FrontController
{
    /**
     * @var CompareService
     *
     */

    protected $compare;

    /**
     * @var ProductRepository
     */
    protected $productRepository;


    /**
     *
     */
    public function init()
    {
        $this->compare = Yii::app()->getComponent('compare');

        $this->productRepository = Yii::app()->getComponent('productRepository');

        parent::init();
    }

    /**
     * @throws CHttpException
     */
    public function actionAdd()
    {
        if (!Yii::app()->getRequest()->getIsPostRequest() || !Yii::app()->getRequest()->getIsAjaxRequest()) {
            throw new CHttpException(404);
        }

        $productId = (int)Yii::app()->getRequest()->getPost('id');

        if (!$productId) {
            throw new CHttpException(404);
        }

        if ($this->compare->add($productId)) {
            Yii::app()->ajax->raw(
                [
                    'result' => true,
                    'data' => Yii::t('CompareModule.compare', 'Success added!'),
                    'count' => $this->compare->count(),
                ]
            );
        }

        Yii::app()->ajax->raw(
            [
                'message' => Yii::t('CompareModule.compare', 'Error =('),
                'result' => false,
                'count' => $this->compare->count(),
            ]
        );
    }

    /**
     * @throws CHttpException
     */
    public function actionRemove()
    {
        if (!Yii::app()->getRequest()->getIsPostRequest() || !Yii::app()->getRequest()->getIsAjaxRequest()) {
            throw new CHttpException(404);
        }

        $productId = (int)Yii::app()->getRequest()->getPost('id');

        if (!$productId) {
            throw new CHttpException(404);
        }

        if ($this->compare->remove($productId)) {
            Yii::app()->ajax->raw(
                [
                    'result' => true,
                    'data' => Yii::t('CompareModule.compare', 'Success removed!'),
                    'count' => $this->compare->count(),
                ]
            );
        }

        Yii::app()->ajax->raw(
            [
                'message' => Yii::t('CompareModule.compare', 'Error =('),
                'result' => false,
                'count' => $this->compare->count(),
            ]
        );
    }

    /**
     *
     */
    public function actionIndex()
    {
        $attributes = [];
        $prices = [];
        $products = $this->compare->products();
        foreach ($products->data as $product) {
            $attributes = array_merge($attributes, $product->getAttributeGroups());
            if (!$prices) {
                $prices = $product->getRentPricesName();
            }
        }

        $this->render('index', ['products' => $products, 'attributes' => $attributes, 'prices' => $prices]);
    }
}