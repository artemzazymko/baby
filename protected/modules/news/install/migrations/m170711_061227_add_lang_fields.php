<?php

class m170711_061227_add_lang_fields extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{news_news}}', 'title_ru', 'varchar(250) NOT NULL AFTER title');
        $this->addColumn('{{news_news}}', 'short_text_ru', 'text AFTER short_text');
        $this->addColumn('{{news_news}}', 'full_text_ru', 'text AFTER full_text');
        $this->addColumn('{{news_news}}', 'keywords_ru', 'varchar(250) NOT NULL AFTER keywords');
        $this->addColumn('{{news_news}}', 'description_ru', 'varchar(250) NOT NULL AFTER description');
    }

    public function safeDown()
    {
        $this->dropColumn('{{news_news}}', 'title_ru');
        $this->dropColumn('{{news_news}}', 'short_text_ru');
        $this->dropColumn('{{news_news}}', 'full_text_ru');
        $this->dropColumn('{{news_news}}', 'keywords_ru');
        $this->dropColumn('{{news_news}}', 'description_ru');
    }
}