<?php
/**
 * News основная моделька для новостей
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2013 amyLabs && Yupe! team
 * @package yupe.modules.news.models
 * @since 0.1
 *
 */

/**
 * This is the model class for table "News".
 *
 * The followings are the available columns in table 'News':
 * @property integer $id
 * @property string $create_time
 * @property string $update_time
 * @property string $date
 * @property string $title
 * @property string $title_ru
 * @property string $slug
 * @property string $short_text
 * @property string $short_text_ru
 * @property string $full_text
 * @property string $full_text_ru
 * @property integer $user_id
 * @property integer $status
 * @property integer $is_protected
 * @property string $link
 * @property string $image
 * @property string $description
 * @property string $description_ru
 * @property string $keywords
 * @property string $keywords_ru
 */

use yupe\components\Event;

/**
 * Class News
 */
class News extends yupe\models\YModel
{
    /**
     *
     */
    const STATUS_DRAFT = 0;
    /**
     *
     */
    const STATUS_PUBLISHED = 1;
    /**
     *
     */
    const STATUS_MODERATION = 2;

    /**
     *
     */
    const PROTECTED_NO = 0;
    /**
     *
     */
    const PROTECTED_YES = 1;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{news_news}}';
    }

    /**
     * Returns the static model of the specified AR class.
     * @param  string $className
     * @return News   the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['title, title_ru, slug, short_text, short_text_ru, full_text, full_text_ru, keywords, keywords_ru, description, description_ru', 'filter', 'filter' => 'trim'],
            ['title, title_ru, slug, keywords, keywords_ru, description, description_ru', 'filter', 'filter' => [new CHtmlPurifier(), 'purify']],
            ['date, title, title_ru, slug, full_text, full_text_ru', 'required', 'on' => ['update', 'insert']],
            ['status, is_protected, category_id', 'numerical', 'integerOnly' => true],
            ['title, title_ru, slug, keywords, keywords_ru', 'length', 'max' => 150],
            ['lang', 'length', 'max' => 2],
            ['lang', 'default', 'value' => Yii::app()->sourceLanguage],
            ['lang', 'in', 'range' => array_keys(Yii::app()->getModule('yupe')->getLanguagesList())],
            ['status', 'in', 'range' => array_keys($this->getStatusList())],
            ['slug', 'yupe\components\validators\YUniqueSlugValidator'],
            ['description, description_ru', 'length', 'max' => 250],
            ['link', 'length', 'max' => 250],
            ['link', 'yupe\components\validators\YUrlValidator'],
            [
                'slug',
                'yupe\components\validators\YSLugValidator',
                'message' => Yii::t('NewsModule.news', 'Bad characters in {attribute} field')
            ],
            ['category_id', 'default', 'setOnEmpty' => true, 'value' => null],
            [
                'id, keywords, keywords_ru, description, description_ru, create_time, update_time, date, title, title_ru, slug, short_text, short_text_ru, full_text, full_text_ru, user_id, status, is_protected, lang',
                'safe',
                'on' => 'search'
            ],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $module = Yii::app()->getModule('news');

        return [
            'imageUpload' => [
                'class' => 'yupe\components\behaviors\ImageUploadBehavior',
                'attributeName' => 'image',
                'minSize' => $module->minSize,
                'maxSize' => $module->maxSize,
                'types' => $module->allowedExtensions,
                'uploadPath' => $module->uploadPath,
            ],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'category' => [self::BELONGS_TO, 'Category', 'category_id'],
            'user' => [self::BELONGS_TO, 'User', 'user_id'],
        ];
    }

    /**
     * @return array
     */
    public function scopes()
    {
        return [
            'published' => [
                'condition' => 't.status = :status',
                'params' => [':status' => self::STATUS_PUBLISHED],
            ],
            'protected' => [
                'condition' => 't.is_protected = :is_protected',
                'params' => [':is_protected' => self::PROTECTED_YES],
            ],
            'public' => [
                'condition' => 't.is_protected = :is_protected',
                'params' => [':is_protected' => self::PROTECTED_NO],
            ],
            'recent' => [
                'order' => 'create_time DESC',
                'limit' => 5,
            ]
        ];
    }

    /**
     * @param $num
     * @return $this
     */
    public function last($num)
    {
        $this->getDbCriteria()->mergeWith(
            [
                'order' => 'date DESC',
                'limit' => $num,
            ]
        );

        return $this;
    }

    /**
     * @param $lang
     * @return $this
     */
    public function language($lang)
    {
        $this->getDbCriteria()->mergeWith(
            [
                'condition' => 'lang = :lang',
                'params' => [':lang' => $lang],
            ]
        );

        return $this;
    }

    /**
     * @param $category_id
     * @return $this
     */
    public function category($category_id)
    {
        $this->getDbCriteria()->mergeWith(
            [
                'condition' => 'category_id = :category_id',
                'params' => [':category_id' => $category_id],
            ]
        );

        return $this;
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('NewsModule.news', 'Id'),
            'category_id' => Yii::t('NewsModule.news', 'Category'),
            'create_time' => Yii::t('NewsModule.news', 'Created at'),
            'update_time' => Yii::t('NewsModule.news', 'Updated at'),
            'date' => Yii::t('NewsModule.news', 'Date'),
            'title' => Yii::t('NewsModule.news', 'Title'),
            'title_ru' => Yii::t('NewsModule.news', 'Title ru'),
            'slug' => Yii::t('NewsModule.news', 'Alias'),
            'image' => Yii::t('NewsModule.news', 'Image'),
            'link' => Yii::t('NewsModule.news', 'Link'),
            'lang' => Yii::t('NewsModule.news', 'Language'),
            'short_text' => Yii::t('NewsModule.news', 'Short text'),
            'short_text_ru' => Yii::t('NewsModule.news', 'Short text ru'),
            'full_text' => Yii::t('NewsModule.news', 'Full text'),
            'full_text_ru' => Yii::t('NewsModule.news', 'Full text ru'),
            'user_id' => Yii::t('NewsModule.news', 'Author'),
            'status' => Yii::t('NewsModule.news', 'Status'),
            'is_protected' => Yii::t('NewsModule.news', 'Access only for authorized'),
            'keywords' => Yii::t('NewsModule.news', 'Keywords (SEO)'),
            'keywords_ru' => Yii::t('NewsModule.news', 'Keywords (SEO) ru'),
            'description' => Yii::t('NewsModule.news', 'Description (SEO)'),
            'description_ru' => Yii::t('NewsModule.news', 'Description (SEO) ru'),
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if (!$this->slug) {
            $this->slug = yupe\helpers\YText::translit($this->title);
        }

        if (!$this->lang) {
            $this->lang = Yii::app()->getLanguage();
        }

        return parent::beforeValidate();
    }

    /**
     * @return bool
     */
    public function beforeSave()
    {
        $this->update_time = new CDbExpression('NOW()');
        $this->date = date('Y-m-d', strtotime($this->date));

        if ($this->getIsNewRecord()) {
            $this->create_time = $this->update_time;
            $this->user_id = Yii::app()->getUser()->getId();
        }

        return parent::beforeSave();
    }

    /**
     *
     */
    public function afterSave()
    {
        Yii::app()->eventManager->fire(NewsEvents::NEWS_AFTER_SAVE, new Event($this));

        return parent::afterSave();
    }

    /**
     *
     */
    public function afterDelete()
    {
        Yii::app()->eventManager->fire(NewsEvents::NEWS_AFTER_DELETE, new Event($this));

        parent::afterDelete();
    }

    /**
     *
     */
    public function afterFind()
    {
        $this->date = date('d-m-Y', strtotime($this->date));

        return parent::afterFind();
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria();

        $criteria->compare('t.id', $this->id);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('update_time', $this->update_time, true);
        if ($this->date) {
            $criteria->compare('date', date('Y-m-d', strtotime($this->date)));
        }
        $criteria->compare('title', $this->title, true);
        $criteria->compare('title_ru', $this->title_ru, true);
        $criteria->compare('t.slug', $this->slug, true);
        $criteria->compare('short_text', $this->short_text, true);
        $criteria->compare('short_text_ru', $this->short_text_ru, true);
        $criteria->compare('full_text', $this->full_text, true);
        $criteria->compare('full_text_ru', $this->full_text_ru, true);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('t.status', $this->status);
        $criteria->compare('category_id', $this->category_id, true);
        $criteria->compare('is_protected', $this->is_protected);
        $criteria->compare('t.lang', $this->lang);
        $criteria->with = ['category'];

        return new CActiveDataProvider(get_class($this), [
            'criteria' => $criteria,
            'sort' => ['defaultOrder' => 'date DESC'],
        ]);
    }

    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_DRAFT => Yii::t('NewsModule.news', 'Draft'),
            self::STATUS_PUBLISHED => Yii::t('NewsModule.news', 'Published'),
            self::STATUS_MODERATION => Yii::t('NewsModule.news', 'On moderation'),
        ];
    }

    /**
     * @return mixed|string
     */
    public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t('NewsModule.news', '*unknown*');
    }

    /**
     * @return array
     */
    public function getProtectedStatusList()
    {
        return [
            self::PROTECTED_NO => Yii::t('NewsModule.news', 'no'),
            self::PROTECTED_YES => Yii::t('NewsModule.news', 'yes'),
        ];
    }

    /**
     * @return mixed|string
     */
    public function getProtectedStatus()
    {
        $data = $this->getProtectedStatusList();

        return isset($data[$this->is_protected]) ? $data[$this->is_protected] : Yii::t('NewsModule.news', '*unknown*');
    }

    /**
     * @return string
     */
    public function getCategoryName()
    {
        return ($this->category === null) ? '---' : $this->category->name;
    }

    /**
     * @return string
     */
    public function getFlag()
    {
        return yupe\helpers\YText::langToflag($this->lang);
    }

    /**
     * @return bool
     */
    public function isProtected()
    {
        return $this->is_protected == self::PROTECTED_YES;
    }

    /**
     * @param string $fieldName
     * @return string
     */
    public function getFiledByLang($fieldName = null)
    {
        return ($fieldName . yupe\helpers\Lang::suffix());
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->{$this->getFiledByLang('title')} ?: $this->{$this->getFiledByLang('name')};
    }

    /**
     * @return string
     */
    public function getFullText()
    {
        return $this->{$this->getFiledByLang('full_text')};
    }

    /**
     * @return string
     */
    public function getShortText()
    {
        return $this->{$this->getFiledByLang('short_text')};
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->{$this->getFiledByLang('description')};
    }

    /**
     * @return string
     */
    public function getKeywords()
    {
        return $this->{$this->getFiledByLang('keywords')};
    }
}
