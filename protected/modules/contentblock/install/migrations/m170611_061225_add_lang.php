<?php

class m170611_061225_add_lang extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{contentblock_content_block}}', 'lang', 'CHAR(2) DEFAULT NULL AFTER category_id');

        //ix
        $this->createIndex("ix_{{contentblock_content_block}}_lang", '{{contentblock_content_block}}', "lang", false);
    }

    public function safeDown()
    {
        $this->dropColumn('{{contentblock_content_block}}', 'lang');
    }
}