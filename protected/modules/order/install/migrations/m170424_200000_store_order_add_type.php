<?php

class m170424_200000_store_order_add_type extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{store_order}}', 'type', 'VARCHAR(50) DEFAULT NULL AFTER delivery_price');

        $this->createIndex("ix_{{store_order}}_type", "{{store_order}}", "type", false);
    }

    public function safeDown()
    {
        $this->dropColumn('{{store_order}}', 'type');
    }
}
