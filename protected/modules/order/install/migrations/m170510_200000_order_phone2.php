<?php

class m170510_200000_order_phone2 extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{store_order}}', 'phone2', 'varchar(255) not null default "" after phone');
        $this->addColumn('{{store_order}}', 'address', 'varchar(255) not null default "" after email');
        $this->addColumn('{{store_order}}', 'address2', 'varchar(255) not null default "" after address');
    }

    public function safeDown()
    {
        $this->dropColumn('{{store_order}}', 'phone2');
        $this->dropColumn('{{store_order}}', 'address');
        $this->dropColumn('{{store_order}}', 'address2');
    }
}
