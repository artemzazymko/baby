<?php

class m170504_200000_order_lang_ru extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{store_order_status}}', 'name_ru', 'string NOT NULL after name');

        $this->addColumn('{{store_order_product}}', 'product_name_ru', 'varchar(255) not null after product_name');
        $this->addColumn('{{store_order_product}}', 'variants_text_ru', 'varchar(1024) null after variants_text');
    }

    public function safeDown()
    {
        $this->dropColumn('{{store_order_status}}', 'name_ru');

        $this->dropColumn('{{store_order_product}}', 'product_name_ru');
        $this->dropColumn('{{store_order_product}}', 'variants_text_ru');
    }
}
