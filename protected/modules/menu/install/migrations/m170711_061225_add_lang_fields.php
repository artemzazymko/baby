<?php

class m170711_061225_add_lang_fields extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{menu_menu_item}}', 'title_ru', 'varchar(150) NOT NULL AFTER title');
        $this->addColumn('{{menu_menu_item}}', 'title_attr_ru', 'varchar(150) NOT NULL AFTER title_attr');
        $this->addColumn('{{menu_menu_item}}', 'before_link_ru', 'varchar(150) NOT NULL AFTER before_link');
        $this->addColumn('{{menu_menu_item}}', 'after_link_ru', 'varchar(150) NOT NULL AFTER after_link');
    }

    public function safeDown()
    {
        $this->dropColumn('{{menu_menu_item}}', 'title_ru');
        $this->dropColumn('{{menu_menu_item}}', 'title_attr_ru');
        $this->dropColumn('{{menu_menu_item}}', 'before_link_ru');
        $this->dropColumn('{{menu_menu_item}}', 'after_link_ru');
    }
}