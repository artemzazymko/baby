<?php

class m170604_061225_add_lang_to_menu_item extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{menu_menu_item}}', 'lang', 'CHAR(2) DEFAULT NULL AFTER menu_id');

        //ix
        $this->createIndex("ix_{{menu_menu_item}}_lang", '{{menu_menu_item}}', "lang", false);
    }

    public function safeDown()
    {
        $this->dropColumn('{{menu_menu_item}}', 'lang');
    }
}