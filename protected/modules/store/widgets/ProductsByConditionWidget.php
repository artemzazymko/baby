<?php

Yii::import('application.modules.store.models.StoreCategory');

/**
 * Class CategoryWidget
 *
 * <pre>
 * <?php
 *    $this->widget('application.modules.store.widgets.CategoryByViewWidget');
 * ?>
 * </pre>
 */
class ProductsByConditionWidget extends yupe\widgets\YWidget
{
    /**
     * @var string
     */
    public $view = 'products';

    /**
     * @var string
     */
    public $label;

    /**
     * @var string
     */
    public $condition;

    /**
     * @var string
     */
    public $product;

    /**
     * @var array
     */
    public $htmlOptions = [];

    /**
     * @throws CException
     */
    public function run()
    {
        $products = new Product();

        $this->render($this->view, [
            'products' => $products->published()->findAll([
                'condition' => 't.condition = :condition AND t.is_special = 1',
                'params' => [':condition' => $this->condition],
                'order' => 'sort',
                'limit' => $this->limit,
            ]),
            'label' => $this->label,
        ]);
    }
}
