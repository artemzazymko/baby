<?php

Yii::import('application.modules.store.models.StoreCategory');

/**
 * Class CategoryWidget
 *
 * <pre>
 * <?php
 *    $this->widget('application.modules.store.widgets.CategoryByViewWidget');
 * ?>
 * </pre>
 */
class CategoryByViewWidget extends yupe\widgets\YWidget
{
    /**
     * @var int
     */
    public $parent = 0;

    /**
     * @var int
     */
    public $depth = 1;

    /**
     * @var string
     */
    public $view = 'category-item-widget';

    /**
     * @var string
     */
    public $categoryView = 'home-page';

    /**
     * @var string
     */
    public $category = 'rent';

    /**
     * @var array
     */
    public $htmlOptions = [];

    /**
     * @var StoreCategoryRepository $categoryRepository
     */
    protected $categoryRepository;

    /**
     *
     */
    public function init()
    {
        $this->categoryRepository = Yii::app()->getComponent('categoryRepository');
        parent::init();
    }

    /**
     * @throws CException
     */
    public function run()
    {

        $category = $this->categoryRepository->getByAlias($this->category);

        if (!$category) {
            return;
        }

        $categories = $category->child();

        $this->render($this->view, [
            'categories' => $categories->published()->findAll([
                'order' => 'sort',
                'limit' => $this->limit,
            ]),
            'parent' => $this->category,
        ]);
    }
}
