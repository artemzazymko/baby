<?php

Yii::import('application.modules.store.models.StoreCategory');

/**
 * Class CategoryWidget
 *
 * <pre>
 * <?php
 *    $this->widget('application.modules.store.widgets.CategoryByViewWidget');
 * ?>
 * </pre>
 */
class CategoryByMainWidget extends yupe\widgets\YWidget
{
    /**
     * @var string
     */
    public $view = 'home-page';

    /**
     * @var string
     */
    public $label;

    /**
     * @var string
     */
    public $category;

    /**
     * @var array
     */
    public $htmlOptions = [];

    /**
     * @var StoreCategoryRepository $categoryRepository
     */
    protected $categoryRepository;

    /**
     * @throws CException
     */
    public function run()
    {
        $categories = new StoreCategory();

        $this->render($this->view, [
            'categories' => $categories->published()->findAll([
                'condition' => 'label = :label',
                'params' => [':label' => $this->label],
                'order' => 'sort',
                'limit' => $this->limit,
            ]),
            'parent' => $this->category,
        ]);
    }
}
