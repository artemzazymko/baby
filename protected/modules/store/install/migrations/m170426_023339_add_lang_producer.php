a<?php

class m170426_023339_add_lang_producer extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{store_producer}}', 'lang', 'FLOAT(10, 2) NOT NULL AFTER name');
    }

    public function safeDown()
    {
        $this->dropColumn('{{store_producer}}', 'lang');
    }
}