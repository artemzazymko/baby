<?php

class m170509_081607_store_lang_ru extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{store_attribute_option}}', 'value_ru', 'varchar(255) null default "" after value');
    }

    public function safeDown()
    {
        $this->dropColumn('{{store_attribute_option}}', 'value_ru');
    }
}
