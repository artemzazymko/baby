<?php

class m170217_023338_add_name_ua_product extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{store_product}}', 'name_ua', 'VARCHAR(250) NOT NULL AFTER name');
    }

    public function safeDown()
    {
        $this->dropColumn('{{store_product}}', 'name_ua');
    }
}