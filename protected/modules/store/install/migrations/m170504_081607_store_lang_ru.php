<?php

class m170504_081607_store_lang_ru extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->dropColumn('{{store_product}}', 'name_ua');

        $this->dropColumn('{{store_producer}}', 'name_ua');

        $this->addColumn('{{store_product}}', 'name_ru', 'varchar(250) not null after name');
        $this->addColumn('{{store_product}}', 'description_ru', 'text after description');
        $this->addColumn('{{store_product}}', 'short_description_ru', 'text after short_description');
        $this->addColumn('{{store_product}}', 'data_ru', 'text after data');
        $this->addColumn('{{store_product}}', 'meta_title_ru', 'varchar(250) default null after meta_title');
        $this->addColumn('{{store_product}}', 'meta_keywords_ru', 'varchar(250) default null after meta_keywords');
        $this->addColumn('{{store_product}}', 'meta_description_ru', 'varchar(250) default null after meta_description');
        $this->addColumn('{{store_product}}', 'meta_canonical_ru', 'varchar(255) default null after meta_canonical');
        $this->addColumn('{{store_product}}', 'title_ru', 'varchar(255) default null  after title');
        $this->addColumn('{{store_product}}', 'image_alt_ru', 'varchar(255) default null  after image_alt');
        $this->addColumn('{{store_product}}', 'image_title_ru', 'varchar(255) default null  after image_title');

        $this->addColumn('{{store_product_image}}', 'name_ru', 'varchar(250) not null after name');
        $this->addColumn('{{store_product_image}}', 'title_ru', 'varchar(250) not null  after title');

        $this->addColumn('{{store_product_link_type}}', 'title_ru', 'string not null  after title');

        $this->addColumn('{{store_producer}}', 'name_short_ru', 'varchar(150) not null after name_short');
        $this->addColumn('{{store_producer}}', 'name_ru', 'varchar(250) not null after name');
        $this->addColumn('{{store_producer}}', 'description_ru', 'text after description');
        $this->addColumn('{{store_producer}}', 'short_description_ru', 'text after short_description');
        $this->addColumn('{{store_producer}}', 'meta_title_ru', 'varchar(250) default null after meta_title');
        $this->addColumn('{{store_producer}}', 'meta_keywords_ru', 'varchar(250) default null after meta_keywords');
        $this->addColumn('{{store_producer}}', 'meta_description_ru', 'varchar(250) default null after meta_description');

        $this->addColumn('{{store_product_image_group}}', 'name_ru', 'varchar(255) not null after name');

        $this->addColumn('{{store_attribute_group}}', 'name_ru', 'varchar(255) not null after name');

        $this->addColumn('{{store_attribute}}', 'name_ru', 'varchar(250) not null after name');
        $this->addColumn('{{store_attribute}}', 'title_ru', 'varchar(250) default null after title');
        $this->addColumn('{{store_attribute}}', 'description_ru', 'text after description');

        $this->addColumn('{{store_category}}', 'name_ru', 'varchar(250) not null after name');
        $this->addColumn('{{store_category}}', 'description_ru', 'text after description');
        $this->addColumn('{{store_category}}', 'short_description_ru', 'text after short_description');
        $this->addColumn('{{store_category}}', 'meta_title_ru', 'varchar(250) default null after meta_title');
        $this->addColumn('{{store_category}}', 'meta_keywords_ru', 'varchar(250) default null after meta_keywords');
        $this->addColumn('{{store_category}}', 'meta_description_ru', 'varchar(250) default null after meta_description');
        $this->addColumn('{{store_category}}', 'meta_canonical_ru', 'varchar(255) default null after meta_canonical');
        $this->addColumn('{{store_category}}', 'title_ru', 'varchar(255) default null  after title');
        $this->addColumn('{{store_category}}', 'image_alt_ru', 'varchar(255) default null  after image_alt');
        $this->addColumn('{{store_category}}', 'image_title_ru', 'varchar(255) default null  after image_title');
    }

    public function safeDown()
    {
        $this->addColumn('{{store_product}}', 'name_ua', 'VARCHAR(250) NOT NULL AFTER name');

        $this->addColumn('{{store_producer}}', 'name_ua', 'VARCHAR(250) NOT NULL AFTER name');

        $this->dropColumn('{{store_product}}', 'name_ru');
        $this->dropColumn('{{store_product}}', 'description_ru');
        $this->dropColumn('{{store_product}}', 'short_description_ru');
        $this->dropColumn('{{store_product}}', 'data_ru');
        $this->dropColumn('{{store_product}}', 'meta_title_ru');
        $this->dropColumn('{{store_product}}', 'meta_keywords_ru');
        $this->dropColumn('{{store_product}}', 'meta_description_ru');
        $this->dropColumn('{{store_product}}', 'meta_canonical_ru');
        $this->dropColumn('{{store_product}}', 'title_ru');
        $this->dropColumn('{{store_product}}', 'image_alt_ru');
        $this->dropColumn('{{store_product}}', 'image_title_ru');

        $this->dropColumn('{{store_product_image}}', 'name_ru');
        $this->dropColumn('{{store_product_image}}', 'title_ru');

        $this->dropColumn('{{store_product_link_type}}', 'title_ru');

        $this->dropColumn('{{store_producer}}', 'name_short_ru');
        $this->dropColumn('{{store_producer}}', 'name_ru');
        $this->dropColumn('{{store_producer}}', 'description_ru');
        $this->dropColumn('{{store_producer}}', 'short_description_ru');
        $this->dropColumn('{{store_producer}}', 'meta_title_ru');
        $this->dropColumn('{{store_producer}}', 'meta_keywords_ru');
        $this->dropColumn('{{store_producer}}', 'meta_description_ru');

        $this->dropColumn('{{store_product_image_group}}', 'name_ru');

        $this->dropColumn('{{store_attribute_group}}', 'name_ru');

        $this->dropColumn('{{store_attribute}}', 'name_ru');
        $this->dropColumn('{{store_attribute}}', 'title_ru');
        $this->dropColumn('{{store_attribute}}', 'description_ru');

        $this->dropColumn('{{store_category}}', 'name_ru');
        $this->dropColumn('{{store_category}}', 'description_ru');
        $this->dropColumn('{{store_category}}', 'short_description_ru');
        $this->dropColumn('{{store_category}}', 'meta_title_ru');
        $this->dropColumn('{{store_category}}', 'meta_keywords_ru');
        $this->dropColumn('{{store_category}}', 'meta_description_ru');
        $this->dropColumn('{{store_category}}', 'meta_canonical_ru');
        $this->dropColumn('{{store_category}}', 'title_ru');
        $this->dropColumn('{{store_category}}', 'image_alt_ru');
        $this->dropColumn('{{store_category}}', 'image_title_ru');
    }
}
