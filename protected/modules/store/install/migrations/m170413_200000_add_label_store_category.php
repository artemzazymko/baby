<?php

class m170413_200000_add_label_store_category extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{store_category}}', 'label', 'VARCHAR(250) NOT NULL AFTER name');

        $this->createIndex("ix_{{store_category}}_label", "{{store_category}}", "label", false);
    }

    public function safeDown()
    {
        $this->dropColumn('{{store_category}}', 'label');
    }
}
