a<?php

class m170413_023339_add_delivery_producer extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{store_producer}}', 'delivery', 'FLOAT(10, 2) NOT NULL AFTER name DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn('{{store_producer}}', 'delivery');
    }
}