<?php

class m170217_061224_add_name_ua_producer extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{store_producer}}', 'name_ua', 'VARCHAR(250) NOT NULL AFTER name');
    }

    public function safeDown()
    {
        $this->dropColumn('{{store_producer}}', 'name_ua');
    }
}