<?php

class m170416_200000_add_lang_store_category extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{store_category}}', 'lang', 'VARCHAR(2) NOT NULL AFTER name');

        $this->createIndex("ix_{{store_category}}_lang", "{{store_category}}", "lang", false);

        $this->dropIndex('ux_{{store_category}}_alias', "{{store_category}}");
    }

    public function safeDown()
    {
        $this->dropColumn('{{store_category}}', 'lang');

        $this->createIndex("ux_{{store_category}}_alias", "{{store_category}}", "alias", true);
    }
}
