<?php

class m170413_023338_add_delivery_product extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{store_product}}', 'delivery', 'FLOAT(10, 2) NOT NULL AFTER price DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn('{{store_product}}', 'delivery');
    }
}