<?php

class m170413_023538_add_fields_product extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{store_product}}', 'rent_price', 'TEXT AFTER price');
        $this->addColumn('{{store_product}}', 'lang', 'VARCHAR(2) DEFAULT NULL AFTER sku');
        $this->addColumn('{{store_product}}', 'sort', 'VARCHAR(250) DEFAULT NULL AFTER delivery');
        $this->addColumn('{{store_product}}', 'condition', 'VARCHAR(250) DEFAULT NULL AFTER status');

        $this->createIndex("ix_{{store_product}}_sort_price", "{{store_product}}", "sort", false);
        $this->createIndex("ix_{{store_product}}_condition_price", "{{store_product}}", "condition", false);

        $this->dropIndex("ux_{{store_product}}_alias", "{{store_product}}");
    }

    public function safeDown()
    {
        $this->dropColumn('{{store_product}}', 'rent_price');
        $this->dropColumn('{{store_product}}', 'lang');
        $this->dropColumn('{{store_product}}', 'sort');
        $this->dropColumn('{{store_product}}', 'condition');

        $this->createIndex("ux_{{store_product}}_alias", "{{store_product}}", "alias", true);
    }
}