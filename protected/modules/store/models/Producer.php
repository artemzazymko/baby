<?php

/**
 * @property integer $id
 * @property string $name_short
 * @property string $name
 * @property string $name_ru
 * @property string $delivery
 * @property string $slug
 * @property integer $status
 * @property integer $sort
 * @property string $image
 * @property string $short_description
 * @property string $short_description_ru
 * @property string $description
 * @property string $description_ru
 * @property string $meta_title
 * @property string $meta_title_ru
 * @property string $meta_description
 * @property string $meta_description_ru
 * @property string $meta_keywords
 * @property string $meta_keywords_ru
 * @property string $view
 *
 * @method getImageUrl
 */
class Producer extends yupe\models\YModel
{
    /**
     *
     */
    const STATUS_ZERO = 0;
    /**
     *
     */
    const STATUS_ACTIVE = 1;
    /**
     *
     */
    const STATUS_NOT_ACTIVE = 2;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{store_producer}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['name_short, name_short_ru, name, name_ru, slug, status', 'required'],
            ['name_short, name_short_ru, name, name_ru, delivery, slug, short_description, short_description_ru, description, description_ru', 'filter', 'filter' => 'trim'],
            ['sort', 'numerical', 'integerOnly' => true],
            ['name_short, name_short_ru', 'length', 'max' => 150],
            ['view', 'length', 'max' => 150],
            ['name, name_ru, image, meta_title, meta_title_ru, meta_keywords, meta_keywords_ru, meta_description, meta_description_ru', 'length', 'max' => 250],
            ['short_description, short_description_ru, description, description_ru', 'safe'],
            ['status', 'in', 'range' => array_keys($this->getStatusList())],
            [
                'slug',
                'yupe\components\validators\YSLugValidator',
                'message' => Yii::t('StoreModule.store', 'Illegal characters in {attribute}'),
            ],
            ['slug', 'unique'],
            [
                'id, name_short, name_short_ru, name, name_ru, lang, delivery, type, slug, status, sort, image, short_description, short_description_ru, description, description_ru, meta_title, meta_title_ru, meta_keywords, meta_keywords_ru, meta_description, meta_description_ru',
                'safe',
                'on' => 'search',
            ],
        ];
    }

    /**
     * @return array
     */
    public function scopes()
    {
        return [
            'published' => [
                'condition' => 't.status = :status',
                'params' => [':status' => self::STATUS_ACTIVE],
                'order' => 'sort ASC',
            ],
        ];
    }

    /**
     * @return array
     */
    public function relations()
    {
        return [
            'productCount' => [self::STAT, 'Product', 'producer_id'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('StoreModule.store', 'ID'),
            'name_short' => Yii::t('StoreModule.store', 'Short title'),
            'name_short_ru' => Yii::t('StoreModule.store', 'Short title ru'),
            'name' => Yii::t('StoreModule.store', 'Title'),
            'name_ru' => Yii::t('StoreModule.store', 'Title ru'),
            'delivery' => Yii::t('StoreModule.store', 'Delivery'),
            'slug' => Yii::t('StoreModule.store', 'URL'),
            'status' => Yii::t('StoreModule.store', 'Status'),
            'sort' => Yii::t('StoreModule.store', 'Order'),
            'image' => Yii::t('StoreModule.store', 'Image'),
            'short_description' => Yii::t('StoreModule.store', 'Short description'),
            'short_description_ru' => Yii::t('StoreModule.store', 'Short description ru'),
            'description' => Yii::t('StoreModule.store', 'Description'),
            'description_ru' => Yii::t('StoreModule.store', 'Description ru'),
            'meta_title' => Yii::t('StoreModule.store', 'Meta title'),
            'meta_title_ru' => Yii::t('StoreModule.store', 'Meta title ru'),
            'meta_keywords' => Yii::t('StoreModule.store', 'Meta keywords'),
            'meta_keywords_ru' => Yii::t('StoreModule.store', 'Meta keywords ru'),
            'meta_description' => Yii::t('StoreModule.store', 'Meta description'),
            'meta_description_ru' => Yii::t('StoreModule.store', 'Meta description ru'),
            'view' => Yii::t('StoreModule.store', 'Template'),
        ];
    }


    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name_short', $this->name_short, true);
        $criteria->compare('name_short_ru', $this->name_short_ru, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('name_ru', $this->name_ru, true);
        $criteria->compare('slug', $this->slug, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('sort', $this->sort);
        $criteria->compare('image', $this->image, true);
        $criteria->compare('short_description', $this->short_description, true);
        $criteria->compare('short_description_ru', $this->short_description_ru, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('description_ru', $this->description_ru, true);

        return new CActiveDataProvider(
            $this, [
                'criteria' => $criteria,
                'sort' => ['defaultOrder' => 'sort'],
            ]
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     *
     * @param string $className active record class name.
     * @return Producer the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $module = Yii::app()->getModule('store');

        return [
            'imageUpload' => [
                'class' => 'yupe\components\behaviors\ImageUploadBehavior',
                'attributeName' => 'image',
                'minSize' => $module->minSize,
                'maxSize' => $module->maxSize,
                'types' => $module->allowedExtensions,
                'uploadPath' => $module !== null ? $module->uploadPath.'/producer' : null,
                'resizeOptions' => [
                    'maxWidth' => 900,
                    'maxHeight' => 900,
                ],
            ],
            'sortable' => [
                'class' => 'yupe\components\behaviors\SortableBehavior',
                'attributeName' => 'sort',
            ],
        ];
    }

    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_ZERO => Yii::t('StoreModule.store', 'Not available'),
            self::STATUS_ACTIVE => Yii::t('StoreModule.store', 'Active'),
            self::STATUS_NOT_ACTIVE => Yii::t('StoreModule.store', 'Not active'),
        ];
    }

    /**
     * @return string
     */
    public function getStatusTitle()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t('StoreModule.store', '*unknown*');
    }

    /**
     * @return array
     */
    public function getFormattedList()
    {
        return CHtml::listData(Producer::model()->findAll(), 'id', 'name_short');
    }

    /**
     * @param string $fieldName
     * @return string
     */
    public function getFiledByLang($fieldName = null)
    {
        return ($fieldName . yupe\helpers\Lang::suffix());
    }

    /**
     * @return string
     */
    public function getShortDescription()
    {
        return $this->{$this->getFiledByLang('short_description')};
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->{$this->getFiledByLang('description')};
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->{$this->getFiledByLang('name')};
    }

}
