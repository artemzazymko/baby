<?php

class m170711_061226_add_lang_fields extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{page_page}}', 'title_short_ru', 'varchar(150) AFTER title_short');
        $this->addColumn('{{page_page}}', 'title_ru', 'varchar(250) AFTER title');
        $this->addColumn('{{page_page}}', 'body_ru', 'text NOT NULL AFTER body');
        $this->addColumn('{{page_page}}', 'keywords_ru', 'varchar(250) NOT NULL AFTER keywords');
        $this->addColumn('{{page_page}}', 'description_ru', 'varchar(250) NOT NULL AFTER description');
    }

    public function safeDown()
    {
        $this->dropColumn('{{page_page}}', 'name_ru');
        $this->dropColumn('{{page_page}}', 'title_ru');
        $this->dropColumn('{{page_page}}', 'body_ru');
        $this->dropColumn('{{page_page}}', 'keywords_ru');
        $this->dropColumn('{{page_page}}', 'description_ru');
    }
}