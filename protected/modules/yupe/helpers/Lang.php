<?php
namespace yupe\helpers;

/**
 * Class Lang
 * @package yupe\helpers
 */
class Lang
{
    public static function langs()
    {
        return explode(',', \Yii::app()->getModule('yupe')->availableLanguages);
    }

    public static function suffix($lang = null)
    {
        $lang = $lang ?: \Yii::app()->language;

        return ('uk' == $lang) ? '' : '_' . $lang;
    }

    public static function suffixTag($lang = null)
    {
        $lang = $lang ?: Yii::app()->language;

        return ('uk' == $lang) ? '' : '-' . $lang;
    }

    public static function suffixField($field = null, $lang = null)
    {

    }
}