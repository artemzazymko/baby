/*
 * jQuery Tiny Pub/Sub
 * https://github.com/cowboy/jquery-tiny-pubsub
 *
 * Copyright (c) 2013 "Cowboy" Ben Alman
 * Licensed under the MIT license.
 */

(function($) {

  var o = $({});

  $.subscribe = function() {
    o.on.apply(o, arguments);
  };

  $.unsubscribe = function() {
    o.off.apply(o, arguments);
  };

  $.publish = function() {
    o.trigger.apply(o, arguments);
  };

}(jQuery));

$('.menu-button').click(function() {
  event.preventDefault();
  $('.main__navbar .navbar').toggleClass('on');
});

$(document).ready(function () {
  $( ".top-menu li.submenuItem" )
      .mouseenter(function() {
        $(this).addClass('active');
      })
      .mouseleave(function() {
        $(this).removeClass('active');
      });
  $('.top-menu li.submenuItem > a').bind('click', function(event) {
    event.preventDefault();
    if ($(this).parent().hasClass('active')) {
      $(this).parent().removeClass('active');
    } else {
      $(this).parent().addClass('active');
    }
  });
});