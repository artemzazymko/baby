<?php
/* @var $this CartController */
/* @var $positions Product[] */
/* @var $order Order */
/* @var $coupons Coupon[] */
/* @var $form CActiveForm */

$mainAssets = Yii::app()->getTheme()->getAssetsUrl();

Yii::app()->getClientScript()->registerScriptFile($mainAssets.'/js/store.js');

$this->title = Yii::t('CartModule.cart', 'Cart');
$this->breadcrumbs = [
    Yii::t("CartModule.cart", 'Catalog') => ['/store/product/index'],
    Yii::t("CartModule.cart", 'Cart'),
];
?>

<?php if (Yii::app()->cart->isEmpty()): ?>
    <div class="main__title grid cart-title">
        <h1 class="h2"><?= Yii::t("CartModule.cart", "Cart is empty"); ?></h1>
        <?= Yii::t("CartModule.cart", "There are no products in cart"); ?>
    </div>
<?php else: ?>
    <div class="main__cart-box grid сart  <?= Yii::app()->cart->hasRentCart() ? 'rent' : null ?>">
        <h3 class="vart-zakaz"><?= Yii::t("CartModule.cart", 'Cart'); ?></h3>
        <div class="row">
            <div class="order-box js-cart col-md-12">
                <div class="cart-table col-md-12">
                    <div class="order-box__header order-box__header_black">
                        <div class="cart-list-header">
                            <div class="cart-list__column image"><?= Yii::t("CartModule.cart", "Image"); ?></div>
                            <div class="cart-list__column product"><?= Yii::t(
                                    "CartModule.cart",
                                    "Product"
                                ); ?></div>
                            <?php if(!Yii::app()->cart->hasRentCart()): ?>
                                <div class="cart-list__column count"><?= Yii::t("CartModule.cart", "Count"); ?></div>
                            <?php endif; ?>
                            <div class="cart-list__column delete"><?= Yii::t("CartModule.cart", "Delete"); ?></div>
                            <div class="cart-list__column summ"><?= Yii::t("CartModule.cart", "Sum"); ?></div>
                        </div>
                    </div>
                    <div class="cart-list">
                        <?php foreach ($positions as $position): ?>
                            <div class="cart-list__item">
                                <?php $positionId = $position->getId(); ?>
                                <?php $productUrl = ProductHelper::getUrl($position->getProductModel()); ?>
                                <?= CHtml::hiddenField('OrderProduct['.$positionId.'][product_id]', $position->id); ?>
                                <input type="hidden" class="position-id" value="<?= $positionId; ?>"/>

                                <div class="cart-item js-cart__item">
                                    <div class="cart-item__info image">
                                        <img src="<?= $position->getProductModel()->getImageUrl(90, 90, false); ?>"
                                             class="cart-item__img"/>
                                    </div>
                                    <div class="cart-item__info product">
                                        <div class="cart-item__title">
                                            <a href="<?= $productUrl; ?>" class="cart-item__link"><?= CHtml::encode(
                                                    $position->getName()
                                                ); ?></a>
                                        </div>
                                        <?php foreach ($position->selectedVariants as $variant): ?>
                                            <h6><?= $variant->attribute->title; ?>: <?= $variant->getOptionValue(); ?></h6>
                                            <?= CHtml::hiddenField('OrderProduct[' . $positionId . '][variant_ids][]', $variant->id); ?>
                                        <?php endforeach; ?>
                                    </div>
                                    <div class="cart-item__quantity <?= Yii::app()->cart->hasRentCart() ? 'hidden' : null ?>">
                                        <span data-min-value='1' data-max-value='99' class="spinput js-spinput">
                                            <span class="spinput__minus js-spinput__minus cart-quantity-decrease"
                                                  data-target="#cart_<?= $positionId; ?>"></span>
                                            <?= CHtml::textField(
                                                'OrderProduct['.$positionId.'][quantity]',
                                                $position->getQuantity(),
                                                ['id' => 'cart_'.$positionId, 'class' => 'spinput__value position-count']
                                            ); ?>
                                            <span class="spinput__plus js-spinput__plus cart-quantity-increase"
                                                  data-target="#cart_<?= $positionId; ?>"></span>
                                        </span>
                                    </div>
                                    <div class="cart-item__summ delete">
                                        <div class="cart-item__action">
                                            <a class="js-cart__delete cart-delete-product"
                                               data-position-id="<?= $positionId; ?>">
                                                <i class="fa fa-fw fa-trash-o"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="cart-item__summ summ">
                                        <span class="position-sum-price"><?= $position->getSumPrice(); ?></span>
                                        <span class="ruble"> <?= Yii::t("CartModule.cart", Yii::app()->getModule('store')->currency); ?></span>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="cart-box__subtotal">
                        <?= Yii::t("CartModule.cart", 'Total:') ?>
                        <span id="cart-full-cost-with-shipping">0</span><span class="ruble"> <?= Yii::t(
                                "CartModule.cart",
                                Yii::app()->getModule('store')->currency
                            ); ?></span>
                    </div>
                </div>

                <div class="order-box__bottom">
                    <div class="cart-box__order-button">
                        <?= CHtml::link(Yii::t("CartModule.cart", 'Checkout'), '/checkout', ['class' => 'cart-checkout']); ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
<?php endif; ?>
