<?php
/* @var $this CartController */
/* @var $positions Product[] */
/* @var $order Order */
/* @var $coupons Coupon[] */
/* @var $form CActiveForm */

$mainAssets = Yii::app()->getTheme()->getAssetsUrl();

Yii::app()->getClientScript()->registerScriptFile($mainAssets.'/js/store.js');

$this->title = Yii::t('CartModule.cart', 'Cart');
$this->breadcrumbs = [
    Yii::t("CartModule.cart", 'Catalog') => ['/store/product/index'],
    Yii::t("CartModule.cart", 'Cart'),
];
?>

<div class="error-summary">
    <?= Yii::app()->getUser()->getFlash(yupe\widgets\YFlashMessages::ERROR_MESSAGE); ?>
</div>

<?php if (Yii::app()->cart->isEmpty()): ?>
    <div class="main__title grid cart-title">
        <h1 class="h2"><?= Yii::t("CartModule.cart", "Cart is empty"); ?></h1>
        <?= Yii::t("CartModule.cart", "There are no products in cart"); ?>
    </div>
<?php else: ?>
    <?php
    $form = $this->beginWidget(
        'CActiveForm',
        [
            'action' => ['/order/order/create'],
            'id' => 'order-form',
            'enableAjaxValidation' => true,
            'enableClientValidation' => true,
            'clientOptions' => [
                'validateOnSubmit' => true,
                'validateOnChange' => false,
                'validateOnType' => false,
                'beforeValidate' => 'js:function(form){$(form).find("button[type=\'submit\']").prop("disabled", true); return true;}',
                'afterValidate' => 'js:function(form, data, hasError){$(form).find("button[type=\'submit\']").prop("disabled", false); return !hasError;}',
            ],
            'htmlOptions' => [
                'hideErrorMessage' => false,
            ],
        ]
    ); ?>

    <div class="main__cart-box grid checkout">
        <h3 class="vart-zakaz"><?= Yii::t("CartModule.cart", 'Checkout'); ?></h3>
        <?php if (empty($deliveryTypes)): ?>
            <div class="alert alert-danger">
                <?= Yii::t("CartModule.cart", "Delivery method aren't selected! The ordering is impossible!") ?>
            </div>
        <?php endif; ?>

        <div class="wrapper-cart-box row">
            <?php if (!empty($deliveryTypes)): ?>
                <div class="order-box col-md-4">
                    <div class="order-box__body">
                        <div class="order-box-delivery">
                            <div class="order-box-delivery__address">
                                <h3 class="h3"><i class="fa fa-user" aria-hidden="true"></i> <?= Yii::t("CartModule.cart", "Address"); ?></h3>
                                <div class="order-form">
                                    <div class="order-form__row">
                                        <div class="order-form__item">
                                            <div class="form-group">
                                                <?= $form->labelEx($order, 'name', ['class' => 'form-group__label']); ?>
                                                <div class="form-group__input">
                                                    <?= $form->textField($order, 'name', ['class' => 'input']); ?>
                                                </div>
                                                <div class="form-group__help">
                                                    <?= $form->error($order, 'name'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="order-form__row">
                                        <div class="order-form__item">
                                            <div class="form-group">
                                                <?= $form->labelEx(
                                                    $order,
                                                    'phone',
                                                    ['class' => 'form-group__label']
                                                ); ?>
                                                <div class="form-group__input">
                                                    <?= $form->textField($order, 'phone', ['class' => 'input']); ?>
                                                </div>
                                                <div class="form-group__help">
                                                    <?= $form->error($order, 'phone'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php if(Yii::app()->cart->hasRentCart()): ?>
                                        <div class="order-form__row">
                                            <div class="order-form__item">
                                                <div class="form-group">
                                                    <?= $form->labelEx(
                                                        $order,
                                                        'phone2',
                                                        ['class' => 'form-group__label']
                                                    ); ?>
                                                    <div class="form-group__input">
                                                        <?= $form->textField($order, 'phone2', ['class' => 'input']); ?>
                                                    </div>
                                                    <div class="form-group__help">
                                                        <?= $form->error($order, 'phone2'); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="order-form__row">
                                            <div class="order-form__item">
                                                <div class="form-group">
                                                    <?= $form->labelEx(
                                                        $order,
                                                        'address',
                                                        ['class' => 'form-group__label']
                                                    ); ?>
                                                    <div class="form-group__input">
                                                        <?= $form->textField($order, 'address', ['class' => 'input']); ?>
                                                    </div>
                                                    <div class="form-group__help">
                                                        <?= $form->error($order, 'address'); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <div class="order-form__row" id="address2">
                                        <div class="order-form__item">
                                            <div class="form-group">
                                                <?= $form->labelEx(
                                                    $order,
                                                    'address2',
                                                    ['class' => 'form-group__label required', 'id' => 'order_address2']
                                                ); ?>
                                                <div class="form-group__input">
                                                    <?= $form->textField($order, 'address2', ['class' => 'input']); ?>
                                                </div>
                                                <div class="form-group__help">
                                                    <?= $form->error($order, 'address2'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="order-form__row">
                                        <div class="order-form__item">
                                            <div class="form-group">
                                                <?= $form->labelEx(
                                                    $order,
                                                    'email',
                                                    ['class' => 'form-group__label']
                                                ); ?>
                                                <div class="form-group__input">
                                                    <?= $form->textField($order, 'email', ['class' => 'input']); ?>
                                                </div>
                                                <div class="form-group__help">
                                                    <?= $form->error($order, 'email'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="order-form__row">
                                        <div class="order-form__item">
                                            <div class="form-group">
                                                <?= $form->labelEx(
                                                    $order,
                                                    'comment',
                                                    ['class' => 'form-group__label']
                                                ); ?>
                                                <div class="form-group__input">
                                                    <?= $form->textArea($order, 'comment', ['class' => 'input']); ?>
                                                </div>
                                                <div class="form-group__help">
                                                    <?= $form->error($order, 'comment'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <div class="wrapper-right col-md-8">
                <div class="col-md-12 paym">
                    <?php if (!empty($deliveryTypes)): ?>
                        <div class="order-box col-md-6">
                            <div class="order-box__header order-box__header_normal shipping"><i class="fa fa-truck" aria-hidden="true"></i> <?= Yii::t(
                                    "CartModule.cart",
                                    "Delivery method"
                                ); ?></div>
                            <div class="order-box__body">
                                <div class="order-box-delivery">
                                    <div class="order-box-delivery__type">
                                        <?php foreach ($deliveryTypes as $key => $delivery): ?>
                                            <div class="rich-radio">
                                                <input type="radio" name="Order[delivery_id]"
                                                        id="delivery-<?= $delivery->id; ?>"
                                                        class="rich-radio__input"
                                                        hidden="hidden"
                                                        value="<?= $delivery->id; ?>"
                                                        <?php if ($delivery->id == 2): ?>
                                                            data-price="<?= $deliveryPrice; ?>"
                                                        <?php else: ?>
                                                            data-price="<?= $delivery->price; ?>"
                                                        <?php endif; ?>
                                                        data-free-from="<?= $delivery->free_from; ?>"
                                                        data-available-from="<?= $delivery->available_from; ?>"
                                                        data-separate-payment="<?= $delivery->separate_payment; ?>">
                                                <label for="delivery-<?= $delivery->id; ?>" class="rich-radio__label">
                                                    <div class="rich-radio-body">
                                                        <div class="rich-radio-body__content">
                                                            <div class="rich-radio-body__heading">
                                                            <span class="rich-radio-body__title">
                                                                <?= $delivery->name; ?>
                                                                <?php if ($delivery->id == 2): ?>
                                                                    - <?= $deliveryPrice; ?>
                                                                <?php else: ?>
                                                                    - <?= $delivery->price; ?>
                                                                <?php endif; ?>
                                                                <?= Yii::t(
                                                                    "CartModule.cart",
                                                                    Yii::app()->getModule('store')->currency
                                                                ); ?>
                                                            </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </label>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>


                    <?php if (!empty($paymentTypes)): ?>
                        <div class="order-box col-md-6">
                            <div class="order-box__header order-box__header_normal payment"><i class="fa fa-credit-card" aria-hidden="true"></i> <?= Yii::t(
                                    "CartModule.cart",
                                    "Payment method"
                                ); ?></div>
                            <div class="order-box__body">
                                <div class="order-box-delivery">
                                    <div class="order-box-delivery__type">
                                        <?php foreach ($paymentTypes as $key => $payment): ?>
                                            <div class="rich-radio">
                                                <input type="radio" name="Order[payment_id]"
                                                       id="payment-<?= $payment->id; ?>"
                                                       class="rich-radio__input"
                                                       hidden="hidden"
                                                       value="<?= $payment->id; ?>"
                                                       data-price="0"
                                                       data-free-from=""
                                                       data-available-from=""
                                                       data-separate-payment=""
                                                       checked="">
                                                <label for="payment-<?= $payment->id; ?>" class="rich-radio__label">
                                                    <div class="rich-radio-body">
                                                        <div class="rich-radio-body__content">
                                                            <div class="rich-radio-body__heading">
                                                            <span class="rich-radio-body__title">
                                                                <?= $payment->name; ?>
                                                            </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </label>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="order-box js-cart col-md-12">
                    <div class="order-box__header order-box__header_normal cart"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <?= Yii::t(
                            "CartModule.cart",
                            "Current order"
                        ); ?></div>
                    <div class="cart-table">
                        <div class="order-box__header order-box__header_black">
                            <div class="cart-list-header">
                                <div class="cart-list__column image"><?= Yii::t("CartModule.cart", "Image"); ?></div>
                                <div class="cart-list__column product"><?= Yii::t(
                                        "CartModule.cart",
                                        "Product"
                                    ); ?></div>
                                <div class="cart-list__column summ"><?= Yii::t("CartModule.cart", "Sum"); ?></div>
                            </div>
                        </div>
                        <div class="cart-list">
                            <?php foreach ($positions as $position): ?>
                                <div class="cart-list__item">
                                    <?php $positionId = $position->getId(); ?>
                                    <?php $productUrl = ProductHelper::getUrl($position->getProductModel()); ?>
                                    <?= CHtml::hiddenField('OrderProduct['.$positionId.'][product_id]', $position->id); ?>
                                    <input type="hidden" class="position-id" value="<?= $positionId; ?>"/>

                                    <div class="cart-item js-cart__item">
                                        <div class="cart-item__info image">
                                            <img src="<?= $position->getProductModel()->getImageUrl(90, 90, false); ?>"
                                                 class="cart-item__img"/>
                                        </div>
                                        <div class="cart-item__info product">
                                            <div class="cart-item__title">
                                                <a href="<?= $productUrl; ?>" class="cart-item__link"><?= CHtml::encode(
                                                        $position->getName()
                                                    ); ?></a>
                                            </div>
                                            <?php foreach ($position->selectedVariants as $variant): ?>
                                                <h6><?= $variant->attribute->title; ?>: <?= $variant->getOptionValue(); ?></h6>
                                                <?= CHtml::hiddenField('OrderProduct[' . $positionId . '][variant_ids][]', $variant->id); ?>
                                            <?php endforeach; ?>
                                        </div>
                                        <div class="cart-item__quantity hidden">
                                        <span data-min-value='1' data-max-value='99' class="spinput js-spinput">
                                            <span class="spinput__minus js-spinput__minus cart-quantity-decrease"
                                                  data-target="#cart_<?= $positionId; ?>"></span>
                                            <?= CHtml::textField(
                                                'OrderProduct['.$positionId.'][quantity]',
                                                $position->getQuantity(),
                                                ['id' => 'cart_'.$positionId, 'class' => 'spinput__value position-count']
                                            ); ?>
                                            <span class="spinput__plus js-spinput__plus cart-quantity-increase"
                                                  data-target="#cart_<?= $positionId; ?>"></span>
                                        </span>
                                        </div>
                                        <div class="cart-item__summ summ">
                                            <span class="position-sum-price"><?= $position->getSumPrice(); ?></span>
                                            <span class="ruble"> <?= Yii::t("CartModule.cart", Yii::app()->getModule('store')->currency); ?></span>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <?php if (Yii::app()->hasModule('coupon') and 1 != true): ?>
                            <div class="order-box__coupon">
                                <div class="coupon-box">
                                    <span class="coupon-box__label">
                                        <?= Yii::t("CartModule.cart", "Coupons"); ?>
                                    </span>
                                    <input id="coupon-code" class="input coupon-box__input">
                                    <button class="btn btn_primary coupon-box__button" type="button"
                                            id="add-coupon-code"><?= Yii::t("CartModule.cart", "Add coupon"); ?></button>
                                    <div class="row fast-order__inputs">
                                        <?php foreach ($coupons as $coupon): ?>
                                            <div class="coupon">
                                        <span class="label" title="<?= $coupon->name; ?>">
                                            <?= $coupon->name; ?>
                                            <button type="button" class="btn btn_primary close"
                                                    data-dismiss="alert">&times;</button>
                                            <?= CHtml::hiddenField(
                                                "Order[couponCodes][{$coupon->code}]",
                                                $coupon->code,
                                                [
                                                    'class' => 'coupon-input',
                                                    'data-code' => $coupon->code,
                                                    'data-name' => $coupon->name,
                                                    'data-value' => $coupon->value,
                                                    'data-type' => $coupon->type,
                                                    'data-min-order-price' => $coupon->min_order_price,
                                                    'data-free-shipping' => $coupon->free_shipping,
                                                ]
                                            ); ?>
                                        </span>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="cart-box__subtotal">
                            <?= Yii::t("CartModule.cart", 'Subtotal:') ?>
                            <span id="cart-sub-cost">0</span><span class="ruble"> <?= Yii::t(
                                    "CartModule.cart",
                                    Yii::app()->getModule('store')->currency
                                ); ?></span>
                        </div>
                        <div class="cart-box__subtotal">
                            <?= Yii::t("CartModule.cart", 'Delivery:') ?>
                            <span id="cart-delivery-cost">0</span><span class="ruble"> <?= Yii::t(
                                    "CartModule.cart",
                                    Yii::app()->getModule('store')->currency
                                ); ?></span>
                        </div>
                        <div class="cart-box__subtotal">
                            <?= Yii::t("CartModule.cart", 'Total:') ?>
                            <span id="cart-full-cost-with-shipping">0</span><span class="ruble"> <?= Yii::t(
                                    "CartModule.cart",
                                    Yii::app()->getModule('store')->currency
                                ); ?></span>
                        </div>
                    </div>

                    <div class="order-box__bottom">
                        <div class="cart-box__order-button">
                            <button type="submit" class="btn btn_big btn_primary"><?= Yii::t(
                                    "CartModule.cart",
                                    "Create order"
                                ); ?></button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <?php $this->endWidget(); ?>
<?php endif; ?>
