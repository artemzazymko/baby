<?php
/* @var $data News */
$url = Yii::app()->createUrl('/news/news/view', ['slug' => $data->slug]);
?>
<div class="fast-order__inputs col-sm-10 col-md-8 row">
    <a href="<?= $url; ?>" class="image-item__link col-xs-6" >
        <figure>
            <?php if ($data->image): ?>
                <?= CHtml::image($data->getImageUrl(), $data->getTitle(), ['width'=>150, 'height'=>150]); ?>
            <?php endif; ?>
        </figure>
    </a>
    <div class="new-desk col-xs-6">
        <h4 class="h3"><?= CHtml::link(CHtml::encode($data->getTitle()), $url, ['class' => 'cart-item__link']); ?></h4>
        <p> <?= $data->getShortText(); ?></p>
    </div>
</div>