<?php
$mainAssets = Yii::app()->getTheme()->getAssetsUrl();
Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/store.js');

$this->title = 'Сравнение товаров';
$this->breadcrumbs = [
    Yii::t("StoreModule.store", "Catalog") => ['/store/product/index'],
    Yii::t("CompareModule.compare", "Compare product")
];
?>

<div class="main_compare">
    <h1><?= Yii::t("CompareModule.compare", "Compare product"); ?></h1>
    <div class="compare-table">
        <div class="head">
            <div class="row">
                <div class="head-cell-first cell"></div>
                <?php foreach ($products->data as $product): ?>
                    <div class="head-cell cell">
                        <a href="<?= ProductHelper::getUrl($product); ?>">
                            <span class="name">
                                <?= $product->getName(); ?>
                            </span>
                        </a>
                        <div class="compare_product_vertical__thumbnail">
                            <img src="<?= StoreImage::product($product, 150, 280, false) ?>"
                                 class="product_vertical__img"
                                 alt="<?= CHtml::encode($product->getImageAlt()); ?>"
                                 title="<?= CHtml::encode($product->getImageTitle()); ?>"
                            />
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="compare_body">
            <?php foreach ($prices as $key => $price): ?>
                <?php if ($key != '1-month') continue; ?>
                <div class="compare_body_row">
                    <div class="name-attribute coll">
                        <?=  substr($price['name'], 0, -2); ?>
                    </div>
                    <?php foreach ($products->data as $product): ?>
                        <div class="value-attribute coll">
                            <?php $rentPrice = json_decode($product->rent_price); ?>
                            <?php if ($rentPrice->{$key}->total): ?>
                                <span class="value">
                                    <?php if ($rentPrice->{$key}->totalDiscount): ?>
                                        <s><?= $rentPrice->{$key}->totalDiscount; ?></s>
                                    <?php endif; ?>
                                    <?= $rentPrice->{$key}->total; ?>
                                    <?= Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency); ?>
                                </span>
                            <?php endif; ?>
                        </div>
                    <?php endforeach; ?>

                </div>
            <?php endforeach; ?>
            <?php foreach ($attributes as $key => $items): ?>
                <?php foreach ($items as $attribute): ?>
                    <div class="compare_body_row">
                        <div class="name-attribute coll">
                            <?= CHtml::encode($attribute->{'title' . yupe\helpers\Lang::suffix()}); ?>
                        </div>
                        <?php foreach ($products->data as $product): ?>
                            <div class="value-attribute coll">
                                <?= AttributeRender::renderValue($attribute, $product->attribute($attribute)); ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endforeach; ?>
            <?php endforeach; ?>
        </div>
    </div>
</div>
