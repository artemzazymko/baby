<?php if(false === $compare->has($product->id)):?>
    <a href="javascript:void(0);" class="entry__toolbar-button yupe-store-compare-add" data-id="<?= $product->id;?>"><i class="fa fa-balance-scale"></i></a>
<?php else:?>
    <a href="javascript:void(0);" class="entry__toolbar-button yupe-store-compare-remove text-error" data-id="<?= $product->id;?>"><i class="fa fa-balance-scale added"></i></a>
<?php endif;?>
