<?php if(false === $compare->has($product->id)):?>
    <div class="product-vertical-extra__button yupe-store-compare-add" data-id="<?= $product->id;?>"><i class="fa fa-balance-scale"></i></div>
<?php else:?>
    <div class="product-vertical-extra__button yupe-store-compare-remove text-error" data-id="<?= $product->id;?>"><i class="fa fa-balance-scale added"></i></div>
<?php endif;?>