<?php
/** @var Page $page */

if ($page->layout) {
    $this->layout = "//layouts/{$page->layout}";
}

$this->title = $page->title;
//$this->breadcrumbs = [
//    Yii::t('HomepageModule.homepage', 'Pages'),
//    $page->title
//];
$this->description = !empty($page->description) ? $page->description : Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = !empty($page->keywords) ? $page->keywords : Yii::app()->getModule('yupe')->siteKeyWords
?>

<h1 class="hidden"><?= $page->title; ?></h1>

<div class="notify">
    <?php $this->widget( "application.modules.contentblock.widgets.ContentBlockWidget", ["code" => "notify"]); ?>
</div>

<?php $this->widget('application.modules.store.widgets.CategoryByMainWidget', ['limit' => 6, 'label' => 'home-page']); ?>


<?php $this->widget('application.modules.store.widgets.ProductsByConditionWidget', ['limit' => 3, 'condition' => 'new', 'label' => 'new-products']); ?>

<?php $this->widget('application.modules.store.widgets.ProductsByConditionWidget', ['limit' => 3, 'condition' => 'promo', 'label' => 'promo-products']); ?>
