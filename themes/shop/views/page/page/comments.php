<?php if (Yii::app()->hasModule('comment')): ?>
    <div id="reviews" class="tabs__body js-tab">
        <div class="product-reviews">
            <?php $this->widget('application.modules.comment.widgets.CommentsWidget', [
                'redirectTo' => '/comments',
                'model' => $this,
            ]); ?>
        </div>
    </div>
<?php endif; ?>