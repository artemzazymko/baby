<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->getTitle();
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->getDescription() ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->getKeywords() ?: Yii::app()->getModule('yupe')->siteKeyWords;
?>
<div class="main__title grid">
    <h1 class="h2 pages-title"><?= $model->getTitle(); ?></h1>
</div>
<div class="main__catalog grid">
    <?= $model->getBody(); ?>
</div>
