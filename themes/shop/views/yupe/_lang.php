<?php

$langs = Yii::app()->getModule('yupe')->getLanguagesList();

if (count($langs) <= 1) {
    return;
}

$urlManager = Yii::app()->getUrlManager();

$url = $_SERVER['REQUEST_URI'] . ((isset($_SERVER['QUERY_STRING']) && !empty($_SERVER['QUERY_STRING'])) ? '?' . $_SERVER['QUERY_STRING'] : '');
?>

<?php $i = 1; ?>

<div style="font-size: 11px">
    <?php foreach ($langs as $key => $lang): ?>

        <?php
        $i++;
        $icon = ($key == Yii::app()->language) ? '<i class="iconflags iconflags-' . $lang . '"></i>' : '';
        ?>

        <?php if (Yii::app()->language == $key): ?>
            <span><?= $icon . strtoupper(Yii::t('default', $lang)); ?></span>
        <?php else: ?>
            <?= CHtml::link(
                $icon . strtoupper(Yii::t('default', $lang)),
                $urlManager->replaceLangInUrl($url, $key)
            ); ?>
        <?php endif ?>

        <?php if ($i == count($langs)): ?>|<?php endif; ?>

    <?php endforeach; ?>
</div>