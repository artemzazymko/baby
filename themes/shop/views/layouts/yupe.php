<!DOCTYPE html>
<html lang="<?= Yii::app()->getLanguage(); ?>">

<head>
    <?php
    \yupe\components\TemplateEvent::fire(ShopThemeEvents::HEAD_START);

    Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/styles/slick.css');
    Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/libs/select2/select2.css');
    Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/styles/common.css');
    Yii::app()->getClientScript()->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.css');
    Yii::app()->getComponent('bootstrap');

    Yii::app()->getClientScript()->registerCoreScript('jquery');
    ?>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta http-equiv="Content-Language" content="ru-RU" />
    <meta http-equiv="viewport" content="width=360" /><meta http-equiv="imagetoolbar" content="no" />
    <meta http-equiv="msthemecompatible" content="no" /><meta http-equiv="cleartype" content="on" />
    <meta http-equiv="HandheldFriendly" content="True" /><meta http-equiv="format-detection" content="address=no" />
    <meta http-equiv="apple-mobile-web-app-capable" content="yes" />
    <meta http-equiv="apple-mobile-web-app-status-bar-style" content="black-translucent" />


    <title><?= $this->title;?></title>
    <meta name="description" content="<?= $this->description;?>" />
    <meta name="keywords" content="<?= $this->keywords;?>" />
    <?php if ($this->canonical): ?>
        <link rel="canonical" href="<?= $this->canonical ?>" />
    <?php endif; ?>

    <script type="text/javascript">
        var yupeTokenName = '<?= Yii::app()->getRequest()->csrfTokenName;?>';
        var yupeToken = '<?= Yii::app()->getRequest()->getCsrfToken();?>';
        var yupeCartDeleteProductUrl = '<?= Yii::app()->createUrl('/cart/cart/delete/')?>';
        var yupeCartUpdateUrl = '<?= Yii::app()->createUrl('/cart/cart/update/')?>';
        var yupeCartWidgetUrl = '<?= Yii::app()->createUrl('/cart/cart/widget/')?>';
    </script>
    <?php \yupe\components\TemplateEvent::fire(ShopThemeEvents::HEAD_END);?>
</head>

<body>
<?php \yupe\components\TemplateEvent::fire(ShopThemeEvents::BODY_START);?>
<div class="main">
    <div class="main__header">
        <div class="header grid">
            <?=  $this->renderPartial('//yupe/_lang'); ?>
            <div class="header__item header-logo">
                <a href="<?= Yii::app()->createUrl(Yii::app()->hasModule('homepage') ? '/homepage/hp/index' : '/site/index') ?>" class="header__logo-link">
                    <img src="<?= $this->mainAssets ?>/images/logo.png" class="header-logo-image desktop">
                    <img src="<?= $this->mainAssets ?>/images/logo-mobile.png" class="header-logo-image mobile">
                </a>
            </div>
            <div class="header__item header-description"><?= CHtml::encode(Yii::app()->getModule('yupe')->siteName);?></div>
            <div class="icons-head">
                <div class="navbar__toolbar comparison icon-h">
                    <?php if(Yii::app()->hasModule('favorite')):?>
                        <a href="<?= Yii::app()->createUrl('/favorite/default/index');?>" class="toolbar-button"><span class="toolbar-button__label"><i class="fa fa-heart-o fa-lg fa-fw"></i> Избранное</span><span class="badge badge_light-blue" id="yupe-store-favorite-total"><?= Yii::app()->favorite->count();?></span></a>
                    <?php endif; ?>
                    <?php if(Yii::app()->hasModule('compare')):?>
                        <a href="<?= Yii::app()->createUrl('/compare/default/index');?>" class="toolbar-button"><i class="fa fa-balance-scale fa-lg fa-fw"></i><span class="toolbar-button__label"><?= Yii::t('default', 'Comparison'); ?> (<i id="yupe-store-compare-total"><?= Yii::app()->compare->count();?></i>)</span></a>
                    <?php endif;?>
                </div>
                <?php if (Yii::app()->hasModule('cart')): ?>
                    <div id="shopping-cart-widget">
                        <?php $this->widget('application.modules.cart.widgets.ShoppingCartWidget'); ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="header__item header-phone">
                <?php
                    $phones = explode('  ', Yii::app()->getModule('store')->phone);
                ?>
                <div class="header__phone" data-count="<?= count($phones) ?>">
                    <span><?= Yii::t('default', 'Phones:'); ?></span>
                    <?php foreach ($phones as $phone) { ?>
                        <div class="phone_element"><?= $phone ?></div>
                    <?php } ?>
                </div>
                <?php if (Yii::app()->hasModule('callback')): ?>
                    <?php $this->widget('application.modules.callback.widgets.CallbackWidget'); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="main__navbar">
        <div class="mobile-menu">
            <div class="main__search grid">
                <div class="search-bar">
                    <div class="search-bar__wrapper">
                        <?php $this->widget('application.modules.store.widgets.SearchProductWidget'); ?>
                    </div>
                    <?php if($this->beginCache('store::category::menu', ['duration' => $this->yupe->coreCacheTime])):?>
                        <?php $this->widget('application.modules.store.widgets.CategoryWidget', ['depth' => 2]); ?>
                        <?php $this->endCache();?>
                    <?php endif;?>
                </div>
            </div>
            <a class="menu-button" href="#">Меню <i class="fa fa-bars" aria-hidden="true"></i></a>
        </div>
        <div class="navbar">
            <div class="navbar__wrapper grid">
                <div class="navbar__menu">
                    <?php if (Yii::app()->hasModule('menu')): ?>
                        <?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'top-menu']); ?>
                    <?php endif; ?>
                </div>
                <div class="navbar__personal">
                    <?php if (1 != true): ?>
                        <div class="navbar__user">
                            <?php if (Yii::app()->getUser()->getIsGuest()): ?>
                                <a href="<?= Yii::app()->createUrl('/user/account/login') ?>" class="btn btn_login-button">
                                    <?= Yii::t('UserModule.user', 'Login'); ?>
                                </a>
                            <?php else: ?>
                                <div class="toolbar-button toolbar-button_dropdown">
                                    <span class="toolbar-button__label">
                                        <i class="fa fa-user fa-lg fa-fw"></i> Личный кабинет
                                    </span>
                                    <span class="badge badge_light-blue"></span>

                                    <div class="dropdown-menu">
                                        <div class="dropdown-menu__header"><?= Yii::app()->getUser()->getProfile()->getFullName() ?></div>
                                        <div class="dropdown-menu__item">
                                            <div class="dropdown-menu__link">
                                                <a href="<?= Yii::app()->createUrl('/order/user/index') ?>">Мои заказы</a>
                                            </div>
                                        </div>
                                        <div class="dropdown-menu__item">
                                            <div class="dropdown-menu__link">
                                                <a href="<?= Yii::app()->createUrl('/user/profile/profile') ?>">
                                                    <?= Yii::t('UserModule.user', 'My profile') ?>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="dropdown-menu__separator"></div>
                                        <div class="dropdown-menu__item">
                                            <div class="dropdown-menu__link dropdown-menu__link_exit">
                                                <a href="<?= Yii::app()->createUrl('/user/account/logout') ?>">
                                                    <?= Yii::t('UserModule.user', 'Logout'); ?>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <?php if (1 != true): ?>
        <div class="main__search grid">
            <div class="search-bar">
                <div class="search-bar__wrapper"><a href="javascript:void(0);" data-toggle="#menu-catalog" class="search-bar__catalog-button">Каталог товаров</a>
                    <?php $this->widget('application.modules.store.widgets.SearchProductWidget'); ?>
                </div>
                <?php if($this->beginCache('store::category::menu', ['duration' => $this->yupe->coreCacheTime])):?>
                    <?php $this->widget('application.modules.store.widgets.CategoryWidget', ['depth' => 2]); ?>
                    <?php $this->endCache();?>
                <?php endif;?>
            </div>
        </div>
    <?php endif; ?>
    <div class="main__breadcrumbs grid">
        <div class="breadcrumbs">
            <?php $this->widget(
                'zii.widgets.CBreadcrumbs',
                [
                    'links' => $this->breadcrumbs,
                    'tagName' => 'ul',
                    'separator' => '',
                    'homeLink' => '<li><a href="/">' . Yii::t('YupeModule.yupe', 'Home') . '</a>',
                    'activeLinkTemplate' => '<li><a href="{url}">{label}</a>',
                    'inactiveLinkTemplate' => '<li><a>{label}</a>',
                    'htmlOptions' => [],
                    'encodeLabel' => false
                ]
            );?>
        </div>
    </div>
    <div class="wrapper-main container">
        <?php $this->widget('application.modules.store.widgets.CategoryByViewWidget', ['limit' => 40]); ?>

        <?php if (!((Yii::app()->controller->id == 'product') && (Yii::app()->controller->action->id == 'index')) && !((Yii::app()->controller->id == 'category') && (Yii::app()->controller->action->id == 'view'))): ?>
            <div class="right-main col-sm-9">
<!--                <div class="notify">-->
<!--                    --><?php //$this->widget( "application.modules.contentblock.widgets.ContentBlockWidget", ["code" => "notify"]); ?>
<!--                </div>-->
        <?php endif; ?>

        <?= $content ?>

        <?php if (!((Yii::app()->controller->id == 'product') && (Yii::app()->controller->action->id == 'index')) && !((Yii::app()->controller->id == 'category') && (Yii::app()->controller->action->id == 'view'))): ?>
            </div>
        <?php endif; ?>
    </div>
    <div class="main__footer">
        <div class="footer">
            <div class="grid container">
                <div class="footer__wrap row">
                    <div class="footer__group col-sm-4 col-xs-12 info">
                        <h4>Інформація:</h4>
                        <?php if (Yii::app()->hasModule('menu')): ?>
                            <?php $this->widget('application.modules.menu.widgets.MenuWidget', [
                                'name' => 'footer',
                                'layout' => 'footer'
                            ]); ?>
                        <?php endif; ?>
                    </div>
                    <div class="footer__group col-sm-4 col-xs-12 social">
                        <?php if($this->beginCache('contentblock::category::footer', ['duration' => $this->yupe->coreCacheTime])):?>
<!--                            --><?php //$this->widget('application.modules.store.widgets.CategoryWidget', [
//                                'depth' => 0,
//                                'view' => 'footer'
//                            ]); ?>
                            <?php $this->endCache();?>
                        <?php endif;?>
                        <?php $this->widget(
                            "application.modules.contentblock.widgets.ContentBlockWidget",
                            array("code" => "footer"));
                        ?>
                    </div>
                    <div class="footer__group col-sm-4 col-xs-12 contact">
                        <h4>Телефонуйте!</h4>
                        <div class="footer__item">
                            <?php
                            $phones = explode('<br>', Yii::app()->getModule('store')->phone);
                            $phoneCount = 1;
                            foreach ($phones as $phone) {
                                ?>
                                <div class="<?= 'phone-'.$phoneCount ?>">
                                    <?= CHtml::link($phone, 'tel:'.$phone, ['class' => 'phone-'.$phoneCount]); ?>
                                </div>
                                <?php
                                $phoneCount++;
                            }
                            ?>
                        </div>
                        <div class="footer__item">9:00 - 20:00 щодня</div>
                        <div class="footer__item"><?= CHtml::encode(Yii::app()->getModule('store')->email);?></div>
                    </div>
<!--                    <div class="footer__group">-->
<!--                        <div class="footer__item footer__item_header">Работаем</div>-->
<!--                        <div class="footer__item">Пн–Пт 09:00–20:00</div>-->
<!--                        <div class="footer__item">Сб 09:00–17:00</div>-->
<!--                        <div class="footer__item">Вс выходной</div>-->
<!--                    </div>-->
                </div>
                <div class="footer_copiright">
                    <div class="footer__item">&copy; Lisenya <?= date('Y') . ' ' . Yii::t('default', 'All rights reserved.')?></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php \yupe\components\TemplateEvent::fire(ShopThemeEvents::BODY_END);?>
<div class='notifications top-right' id="notifications"></div>
<?php
Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/index.js', CClientScript::POS_END);
Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/jquery.collapse.js', CClientScript::POS_END);
Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/jquery.collapse_storage.js', CClientScript::POS_END);
Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/jquery.fancybox.js', CClientScript::POS_END);
Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/overlay.js', CClientScript::POS_END);
Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/product-gallery.js', CClientScript::POS_END);
Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/slick.js', CClientScript::POS_END);
Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/slick.min.js', CClientScript::POS_END);
Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/tabs.js', CClientScript::POS_END);
Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/toggle.js', CClientScript::POS_END);
Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/libs/select2/select2.min.js', CClientScript::POS_END);
Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/store.js', CClientScript::POS_END);
?>
</body>
</html>