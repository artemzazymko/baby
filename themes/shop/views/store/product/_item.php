<?php
/**
 * @var Product $data
 */
$classes = ['green', 'blue', 'yellow', 'violet'];
?>
<div class="catalog__item <?= $data->sort ?>  <?= $data->condition ?> col-md-4 col-xs-6 <?= $classes[array_rand($classes)] ?>">
    <article class="product-vertical">
        <a href="<?= ProductHelper::getUrl($data); ?>">
            <div class="product-vertical__thumbnail">
                <?php if(!empty( $data->condition)): ?><span class="condition"><?= Yii::t("StoreModule.store", $data->condition); ?></span><?php endif; ?>
                <img src="<?= StoreImage::product($data, 150, 280, false) ?>"
                     class="product-vertical__img"
                     alt="<?= CHtml::encode($data->getImageAlt()); ?>"
                     title="<?= CHtml::encode($data->getImageTitle()); ?>"
                />
                <?php list($price, $currency) = $data->getPriceBySort(); ?>
                <div class="product-price"><?= $price ?><span class="ruble"> <?= Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency . ($currency ? ' ' . $currency : null)); ?></span></div>
            </div>
        </a>
        <div class="product-vertical__content">
            <a href="<?= ProductHelper::getUrl($data); ?>" class="product-vertical__title"><?= CHtml::encode($data->getName()); ?></a>
            <a href="<?= ProductHelper::getUrl($data); ?>" class="details"> <?= Yii::t("StoreModule.store", 'More detailed...'); ?></a>
            <?php if(Yii::app()->hasModule('compare')):?>
                <?php $this->widget('application.modules.compare.widgets.CompareControl', ['product' => $data]);?>
            <?php endif;?>
        </div>
    </article>
</div>
