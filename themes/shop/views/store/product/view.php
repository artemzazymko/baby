<?php

/* @var $product Product */

$this->title = $product->getMetaTitle();
$this->description = $product->getMetaDescription();
$this->keywords = $product->getMetaKeywords();
$this->canonical = $product->getMetaCanonical();

Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/styles/jquery.rating.css');

Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/store.js', CClientScript::POS_END);
Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/jquery.rating.min.js', CClientScript::POS_END);

$this->breadcrumbs = array_merge(
    [Yii::t("StoreModule.store", 'Catalog') => ['/store/product/index']],
    $product->category ? $product->category->getBreadcrumbs(true) : [],
    [CHtml::encode($product->getName())]
);
?>
<div class="heder-title">
    <h1 class="h1"><?= CHtml::encode($product->getTitle()); ?></h1>
</div>
<div class="main__product-description">
    <div class="product-description row">
        <div class="product-description__img-block col-md-6">
            <div class="product-gallery js-product-gallery">
                <div class="product-gallery__body">
                    <div class="product-gallery__nav">
                        <a href="<?= StoreImage::product($product); ?>" rel="group" data-product-thumbnail
                           class="product-gallery__nav-item">
                            <img src="<?= $product->getImageUrl(60, 60, false); ?>" alt=""
                                 class="product-gallery__nav-img">
                        </a>
                        <?php foreach ($product->getImages() as $key => $image): ?>
                            <a href="<?= $image->getImageUrl(); ?>" rel="group" data-product-thumbnail
                               class="product-gallery__nav-item">
                                <img src="<?= $image->getImageUrl(60, 60, false); ?>" alt=""
                                     class="product-gallery__nav-img">
                            </a>
                        <?php endforeach; ?>
                    </div>
                    <div data-product-image class="product-gallery__img-wrap">
                        <img src="<?= StoreImage::product($product); ?>"
                             class="product-gallery__main-img"
                             alt="<?= CHtml::encode($product->getImageAlt()); ?>"
                             title="<?= CHtml::encode($product->getImageTitle()); ?>"
                        >
                    </div>
                    <?php if ($product->isSpecial()): ?>
                        <div class="product-gallery__label hidden">
                            <div class="product-label product-label_hit">
                                <div class="product-label__text">Хит</div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="product-description__entry col-md-6">
<!--            <div id="rating_1">-->
<!--                <input type="hidden" class="val" value="1.75"/>-->
<!--                <input type="hidden" class="votes" value="2"/>-->
<!--            </div>-->
            <div class="entry">
                <?php if ($product->sku): ?>
                    <dl class="entry__product-spec-item">
                        <dt class="product-spec-item__name">
                            <span class="product-spec-item__name-inner">
                                <?= Yii::t("StoreModule.store", "SKU"); ?>
                            </span>
                        </dt>
                        <dd class="product-spec-item__value">
                            <span class="product-spec-item__value-inner">
                                <?= CHtml::encode($product->sku); ?>
                            </span>
                        </dd>
                    </dl>
                <?php endif; ?>
                <div class="entry__toolbar hidden">
                    <div class="entry__toolbar-right">
                        <?php if(Yii::app()->hasModule('favorite')):?>
                            <?php $this->widget('application.modules.favorite.widgets.FavoriteControl', ['product' => $product, 'view' => '_in-product']);?>
                        <?php endif;?>
                        <?php if(Yii::app()->hasModule('compare')):?>
                            <?php $this->widget('application.modules.compare.widgets.CompareControl', ['product' => $product, 'view' => '_in-product']);?>
                        <?php endif;?>
                    </div>
                </div>
                <div class="entry__wysiwyg hidden">
                    <div class="wysiwyg">
                        <?= $product->getShortDescription(); ?>
                    </div>
                </div>
                <form action="<?= Yii::app()->createUrl('cart/cart/add'); ?>" method="post">
                    <input type="hidden" name="Product[id]" value="<?= $product->id; ?>"/>
                    <?= CHtml::hiddenField(
                        Yii::app()->getRequest()->csrfTokenName,
                        Yii::app()->getRequest()->csrfToken
                    ); ?>

                    <?php if ($product->getVariantsGroup()): ?>

                        <div class="entry__title hidden">
                            <h2 class="h3 h_upcase"><?= Yii::t("StoreModule.store", "Variants"); ?></h2>
                        </div>

                        <div class="entry__variants hidden">
                            <?php foreach ($product->getVariantsGroup() as $title => $variantsGroup): ?>
                                <div class="entry__variant">
<!--                                    <div class="entry__variant-title">--><?//= CHtml::encode($title); ?><!--</div>-->
                                    <div class="entry__variant-value">
                                        <?=
                                        CHtml::dropDownList('ProductVariant[]', null, CHtml::listData($variantsGroup, 'id', 'optionValue'), [
                                            'empty' => '--выберите--',
                                            'class' => 'js-select2 entry__variant-value-select noborder',
                                            'options' => $product->getVariantsOptions()
                                        ]); ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>

                    <?php if ($product->hasTypeRent()): ?>
                        <a class="under-terms" href="/yak-otrimati-rich-na-prokat"><?= Yii::t("StoreModule.store", "Under the terms of hire"); ?></a>
                        <div class="entry__price rent"><?= Yii::t("StoreModule.store", "Down payment is"); ?>:
                    <?php else: ?>
                        <div class="entry__price"><?= Yii::t("StoreModule.store", "Price"); ?>:
                    <?php endif; ?>
                        <div class="product-price">
                            <input type="hidden" id="base-price"
                                   value="<?= round($product->getResultPrice(), 2); ?>"/>
                            <span id="result-price"><?= round($product->getResultPrice(), 2); ?></span>
                            <span class="ruble"> <?= Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency); ?></span>
                            <?php if ($product->hasDiscount() && !1): ?>
                                <div class="product-price product-price_old"><?= round($product->getBasePrice(), 2) ?><span class="ruble"> <?= Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency); ?></span></div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php if ($product->hasTypeRent()): ?>
                        <div class="entry__rent">
                            <p class="price-pro">
                                <?= Yii::t("StoreModule.store", "Cost of hire"); ?>
                                <small><?= Yii::t("StoreModule.store", "Deducted from the advance"); ?></small>
                            </p>
                        </div>
                        <?php if ($product->getRentPrice()): ?>
                            <div class="entry__rent_price">
                            <?php foreach ($product->getRentPricesList() as $rentPrice): ?>
                                <?php if ($rentPrice->total): ?>
                                    <div class="payments row">
                                        <div class="entry__rent_price_name col-xs-8">
                                            <?= $rentPrice->name; ?>
                                        </div>
                                        <?php if ($rentPrice->totalDiscount): ?>
                                            <div class="entry__rent_price_total_discount col-xs-2">
                                                <?= $rentPrice->totalDiscount; ?>
                                            </div>
                                        <?php endif; ?>
                                        <div class="entry__rent_price_total col-xs-<?= $rentPrice->totalDiscount ? '2 total_discount' : 4; ?>">
                                            <span class="rent-total">
                                                <?= $rentPrice->total; ?>
                                            </span>
                                            <?= Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency); ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                    <?php if (Yii::app()->hasModule('order')): ?>
                        <div class="entry__cart-button">
                            <?php if(!Yii::app()->cart->itemAt($product->id)):?>
                            <button class="btn btn_cart" id="add-product-to-cart"
                                    data-loading-text="<?= Yii::t("StoreModule.store", "Adding"); ?>">
                                <?php if ($product->hasTypeRent()): ?>
                                    <?= Yii::t("StoreModule.store", "Add to cart rent"); ?>
                                <?php else: ?>
                                    <?= Yii::t("StoreModule.store", "Add to cart depot"); ?>
                                <?php endif; ?>
                            </button>
                            <?php endif;?>
                        </div>
                        <?php if ($product->delivery): ?>
                            <div class="entry__cart-delivery">
                                <span class="delivery-title"><strong><?= Yii::t("StoreModule.store", "Delivery total"); ?></strong></span>
                                <span class="delivery-total"><i><?= $product->delivery; ?></i> <?= Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency); ?></span>
                            </div>
                        <?php endif;?>

                    <?php endif; ?>
                    <?php if ($product->hasTypeRent()): ?>
                        <ul class="info-block">
                            <li><?= Yii::t("StoreModule.store", "Advance payment"); ?></li>
                            <li><?= Yii::t("StoreModule.store", "The remaining funds advance"); ?></li>
                            <li><?= Yii::t("StoreModule.store", "We do not have the minimum"); ?></li>
                        </ul>
                    <?php endif; ?>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="main__product-tabs grid">
    <div class="tabs tabs_classic tabs_gray js-tabs">
        <ul data-nav="data-nav" class="tabs__list">
            <?php if (!empty($product->getDescription())): ?>
                <li class="tabs__item">
                    <a href="#description" class="tabs__link"><?= Yii::t("StoreModule.store", "Description"); ?></a>
                </li>
            <?php endif; ?>
            <li class="tabs__item">
                <a href="#spec" class="tabs__link"><?= Yii::t("StoreModule.store", "Characteristics"); ?></a>
            </li>
            <?php if (Yii::app()->hasModule('comment')): ?>
                <li class="tabs__item">
                    <a href="#reviews" class="tabs__link"><?= Yii::t("StoreModule.store", "Comments"); ?></a>
                </li>
            <?php endif; ?>
        </ul>
        <div class="tabs__bodies js-tabs-bodies">
            <div id="spec" class="tabs__body js-tab">
                <div class="product-spec">
                    <div class="product-spec__body">
                        <?php if ($product->producer_id): ?>
                            <dl class="product-spec-item">
                                <dt class="product-spec-item__name">
                                    <span class="product-spec-item__name-inner">
                                        <?= Yii::t("StoreModule.store", "Producer"); ?>
                                    </span>
                                </dt>
                                <dd class="product-spec-item__value">
                                    <span class="product-spec-item__value-inner">
                                        <?php if($product->getProducerName()):?>
                                            <?= CHtml::link($product->getProducerName(), ['/store/producer/view', 'slug' => $product->producer->slug]);?>
                                        <?php endif;?>
                                    </span>
                                </dd>
                            </dl>
                        <?php endif; ?>

                        <?php if ($product->sku): ?>
                            <dl class="product-spec-item">
                                <dt class="product-spec-item__name">
                                    <span class="product-spec-item__name-inner">
                                        <?= Yii::t("StoreModule.store", "SKU"); ?>
                                    </span>
                                </dt>
                                <dd class="product-spec-item__value">
                                    <span class="product-spec-item__value-inner">
                                        <?= CHtml::encode($product->sku); ?>
                                    </span>
                                </dd>
                            </dl>
                        <?php endif; ?>

                        <?php if ($product->length): ?>
                            <dl class="product-spec-item">
                                <dt class="product-spec-item__name">
                                    <span class="product-spec-item__name-inner">
                                        <?= Yii::t("StoreModule.store", "Length"); ?>
                                    </span>
                                </dt>
                                <dd class="product-spec-item__value">
                                    <span class="product-spec-item__value-inner">
                                        <?= round($product->length, 2); ?> <?= Yii::t("StoreModule.store", "m"); ?>
                                    </span>
                                </dd>
                            </dl>
                        <?php endif; ?>

                        <?php if ($product->width): ?>
                            <dl class="product-spec-item">
                                <dt class="product-spec-item__name">
                                    <span class="product-spec-item__name-inner">
                                        <?= Yii::t("StoreModule.store", "Width"); ?>
                                    </span>
                                </dt>
                                <dd class="product-spec-item__value">
                                    <span class="product-spec-item__value-inner">
                                        <?= round($product->width, 2); ?> <?= Yii::t("StoreModule.store", "m"); ?>
                                    </span>
                                </dd>
                            </dl>
                        <?php endif; ?>

                        <?php if ($product->height): ?>
                            <dl class="product-spec-item">
                                <dt class="product-spec-item__name">
                                    <span class="product-spec-item__name-inner">
                                        <?= Yii::t("StoreModule.store", "Height"); ?>
                                    </span>
                                </dt>
                                <dd class="product-spec-item__value">
                                    <span class="product-spec-item__value-inner">
                                        <?= round($product->height, 2); ?> <?= Yii::t("StoreModule.store", "m"); ?>
                                    </span>
                                </dd>
                            </dl>
                        <?php endif; ?>

                        <?php if ($product->weight): ?>
                            <dl class="product-spec-item">
                                <dt class="product-spec-item__name">
                                    <span class="product-spec-item__name-inner">
                                        <?= Yii::t("StoreModule.store", "Weight"); ?>
                                    </span>
                                </dt>
                                <dd class="product-spec-item__value">
                                    <span class="product-spec-item__value-inner">
                                        <?= round($product->weight, 2); ?> <?= Yii::t("StoreModule.store", "m"); ?>
                                    </span>
                                </dd>
                            </dl>
                        <?php endif; ?>

                        <?php foreach ($product->getAttributeGroups() as $groupName => $items): ?>
                            <?php foreach ($items as $attribute): ?>
                                <?php if (!empty(AttributeRender::renderValue($attribute, $product->attribute($attribute)))): ?>
                                    <dl class="product-spec-item">
                                        <dt class="product-spec-item__name">
                                            <span class="product-spec-item__name-inner">
                                                <?= CHtml::encode($attribute->{'title' . yupe\helpers\Lang::suffix()}); ?>
                                            </span>
                                        </dt>
                                        <dd class="product-spec-item__value">
                                            <span class="product-spec-item__value-inner">
                                                <?= AttributeRender::renderValue($attribute, $product->attribute($attribute)); ?>
                                            </span>
                                        </dd>
                                    </dl>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <?php if (!empty($product->getDescription())): ?>
                <div id="description" class="tabs__body js-tab">
                    <div class="wysiwyg">
                        <?= $product->getDescription() ?>
                    </div>
                </div>
            <?php endif; ?>
            <?php if (Yii::app()->hasModule('comment')): ?>
                <div id="reviews" class="tabs__body js-tab">
                    <div class="product-reviews">
                        <?php $this->widget('application.modules.comment.widgets.CommentsWidget', [
                            'redirectTo' => ProductHelper::getUrl($product),
                            'model' => $product,
                        ]); ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<script type="text/javascript">
//    $(function(){
//        $('#rating_1').rating({
//            fx: 'full',
//            image: '//' + $(location).attr('host') + '/images/stars.png',
//            loader: '//' + $(location).attr('host') + '/images/ajax-loader.gif',
//            width: 32,
//            type: 'GET',
//            url: 'rating.php'
//        });
//    })
</script>

<?php $this->widget('application.modules.store.widgets.LinkedProductsWidget', ['product' => $product, 'code' => null,]); ?>

