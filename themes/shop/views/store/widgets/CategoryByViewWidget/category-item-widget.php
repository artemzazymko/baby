<?php if (!empty($categories) && !((Yii::app()->controller->id == 'product') && (Yii::app()->controller->action->id == 'index')) && !((Yii::app()->controller->id == 'category') && (Yii::app()->controller->action->id == 'view'))):?>
    <div class="left-main col-sm-3">
        <div class="row">
            <div class="left-block-categories col-xs-12">
                <?php $this->widget('application.modules.store.widgets.SearchProductWidget', ['view' => 'search-product-left-block']); ?>
            </div>
        </div>
        <div class="row">
            <div data-collapse="persist" id="filter-category" class="categories-block col-xs-12">
                <div class="filter-block__title"><?= Yii::t('StoreModule.store', 'Categories');?></div>
                <div class="filter-block__body">
                    <div class="filter-block__list filter-block__list_column_2">
                        <?php foreach($categories as $category):?>
                            <div class="filter-block__list-item">
                                <?= CHtml::link(CHtml::encode($category->getName()), StoreCategoryHelper::getUrl($category)) ?>
                            </div>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div data-collapse="persist" id="filter-category" class="categories-block col-xs-12">
                <?php $this->widget( "application.modules.contentblock.widgets.ContentBlockWidget", ["code" => "widget-left"]); ?>
            </div>
        </div>
    </div>
<?php endif;?>
