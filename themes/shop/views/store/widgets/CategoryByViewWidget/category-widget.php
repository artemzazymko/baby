<?php if (!empty($categories)): ?>
    <div class="row">
        <div data-collapse="persist" id="filter-category" class="categories-block col-xs-12">
            <div class="filter-block__title"><?= Yii::t('StoreModule.store', 'Categories');?></div>
            <div class="filter-block__body">
                <div class="filter-block__list filter-block__list_column_2">
                    <?php foreach($categories as $category):?>
                        <div class="filter-block__list-item">
                            <?= CHtml::link(CHtml::encode($category->getName()), StoreCategoryHelper::getUrl($category)) ?>
                        </div>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>