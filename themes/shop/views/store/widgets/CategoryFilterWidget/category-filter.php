<?php if(!empty($categories)):?>
    <div data-collapse="persist" id="filter-category" class="filter-block">
        <div class="filter-block__title"><?= Yii::t('StoreModule.store', 'Categories');?></div>
        <div class="filter-block__body">
            <div class="filter-block__list filter-block__list_column_2">
                <?php foreach($categories as $category):?>
                    <?= CHtml::link(CHtml::encode($category->getName()), StoreCategoryHelper::getUrl($category)) ?>
                <?php endforeach;?>
            </div>
        </div>
    </div>
<?php endif;?>
