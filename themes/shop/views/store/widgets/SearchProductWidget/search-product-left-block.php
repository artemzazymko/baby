<?php $form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    [
        'action' => ['/store/product/index'],
        'method' => 'GET',
    ]
) ?>
<div class="input-group">
    <?= CHtml::textField(
        AttributeFilter::MAIN_SEARCH_QUERY_NAME,
        CHtml::encode(Yii::app()->getRequest()->getQuery(AttributeFilter::MAIN_SEARCH_QUERY_NAME)),
        ['class' => 'search-bar__input form-control', 'aria-describedby' => 'basic-addon2']
    ); ?>
    <div class="search-button input-group-addon" id="basic-addon2"><input type="submit" value="search" class="btn btn_wide"/></div>

</div>

<?php $this->endWidget(); ?>
