<?php $classes = ['green', 'blue', 'yellow', 'violet']; ?>
<?php if(!empty($categories)):?>
    <div class="categories-home-page">
        <div class="categories-home-page-body row">
            <?php foreach($categories as $category): ?>
            <div class="categories-home-page-item col-md-4 col-xs-6 <?= $classes[array_rand($classes)] ?>">
                <figure>
                    <a href="<?= StoreCategoryHelper::getUrl($category); ?>">
                        <div class="category-vertical__thumbnail">
                            <img src="<?= StoreImage::category($category, 150, 280, false) ?>"
                                 class="category-vertical__img"
                                 alt="<?= CHtml::encode($category->getImageAlt()); ?>"
                                 title="<?= CHtml::encode($category->getImageTitle()); ?>"
                            />
                        </div>
                    </a>
                </figure>
                <div class="filter-block__list-item">
                    <?= CHtml::link(CHtml::encode($category->getName()), StoreCategoryHelper::getUrl($category)) ?>
                </div>
            </div>
            <?php endforeach;?>
        </div>
    </div>
<?php endif;?>
