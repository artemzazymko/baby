<?php $classes = ['green', 'blue', 'yellow', 'violet']; ?>
<?php if(!empty($products)):?>
    <h2 class="<?= $label; ?>"><?= Yii::t('StoreModule.store', $label); ?></h2>
    <div class="products-home-page <?= $label; ?>">
        <div class="products-home-page-body row">
            <?php foreach($products as $product): ?>
                <div class="products-home-page-item col-sm-6 <?= $classes[array_rand($classes)] ?>">
                    <figure class="col-xs-6 col-sm-8 col-md-6">
                        <a href="<?= ProductHelper::getUrl($product); ?>">
                            <div class="product-vertical__thumbnail">
                                <img src="<?= StoreImage::product($product, 150, 280, false) ?>"
                                     class="product-vertical__img"
                                     alt="<?= CHtml::encode($product->getImageAlt()); ?>"
                                     title="<?= CHtml::encode($product->getImageTitle()); ?>"
                                />
                            </div>
                        </a>
                    </figure>
                    <div class="filter-block__list-item col-xs-6 col-sm-4 col-md-6">
                        <?= CHtml::link(CHtml::encode($product->getName()), ProductHelper::getUrl($product)) ?>
                        <?= $product->getShortDescription(); ?>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
    </div>
<?php endif;?>
