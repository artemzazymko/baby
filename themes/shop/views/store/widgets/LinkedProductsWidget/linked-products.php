<?php if ($dataProvider->getTotalItemCount()): ?>
    <div class="main__recently-viewed-slider">
        <div class="grid">
            <div class="h3"><?= Yii::t("StoreModule.store", "Product take"); ?></div>
            <div data-show='3' data-scroll='3' data-infinite="data-infinite" class="">
                <?php $this->widget(
                    'zii.widgets.CListView',
                    [
                        'dataProvider' => $dataProvider,
                        'template' => '{items}',
                        'itemView' => '_item',
                        'itemsCssClass' => 'h-slider__slides js-slick__container',
                        'cssFile' => false,
                        'pager' => false,
                    ]
                ); ?>
            </div>
        </div>
    </div>
<?php endif; ?>
