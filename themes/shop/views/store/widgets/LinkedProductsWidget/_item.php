<?php
/**
 * @var Product $data
 */
?>
<div class="catalog__item <?= $data->sort ?>  <?= $data->condition ?> col-md-4 col-xs-6">
    <article class="product-vertical">
        <a href="<?= ProductHelper::getUrl($data); ?>">
            <div class="product-vertical__thumbnail">
                <?php if(!empty( $data->condition)): ?><span class="condition"><?= Yii::t("StoreModule.store", $data->condition); ?></span><?php endif; ?>
                <img src="<?= StoreImage::product($data, 150, 280, false) ?>"
                     class="product-vertical__img"
                     alt="<?= CHtml::encode($data->getImageAlt()); ?>"
                     title="<?= CHtml::encode($data->getImageTitle()); ?>"
                />
                <div class="product-price"><?= $data->getResultPrice() ?><span class="ruble"> <?= Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency); ?></span></div>
            </div>
        </a>
        <div class="product-vertical__content">
            <a href="<?= ProductHelper::getUrl($data); ?>" class="product-vertical__title"><?= CHtml::encode($data->getName()); ?></a>
        </div>
    </article>
</div>
