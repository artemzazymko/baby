<?php
/* @var $category StoreCategory */
/* @var $dataProvider CActiveDataProvider */

$mainAssets = Yii::app()->getTheme()->getAssetsUrl();
Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/store.js');

$this->title =  $category->getMetaTile();
$this->description = $category->getMetaDescription();
$this->keywords =  $category->getMetaKeywords();
$this->canonical = $category->getMetaCanonical();

$this->breadcrumbs = [Yii::t("StoreModule.store", "Catalog") => ['/store/product/index']];

$this->breadcrumbs = array_merge(
    $this->breadcrumbs,
    $category->getBreadcrumbs(true)
);

?>
<div class="main__catalog">
    <div class="row">
        <div class="col-sm-3">
            <div class="catalog-filter">
                <form id="store-filter" name="store-filter" method="get">
                    <?php $this->widget('application.modules.store.widgets.filters.QFilterWidget'); ?>
                    <?php //$this->widget('application.modules.store.widgets.filters.CategoryFilterWidget', ['limit' => 30, 'category' => $category]); ?>
                    <?php $this->widget('application.modules.store.widgets.CategoryByViewWidget', ['limit' => 40, 'view' => 'category-widget']); ?>
                    <?php $this->widget('application.modules.store.widgets.filters.PriceFilterWidget'); ?>
                    <?php $this->widget('application.modules.store.widgets.filters.ProducerFilterWidget', ['limit' => 30, 'category' => $category]); ?>
                    <?php $this->widget('application.modules.store.widgets.filters.FilterBlockWidget', [
                        'category' => $category
                    ]); ?>
                    <div class="row">
                        <div data-collapse="persist" id="filter-category" class="categories-block col-xs-12">
                            <?php $this->widget( "application.modules.contentblock.widgets.ContentBlockWidget", ["code" => "widget-left"]); ?>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-sm-9">

            <?php //$this->widget( "application.modules.contentblock.widgets.ContentBlockWidget", ["code" => "notify"]); ?>

            <div class="entry__title">
                <h1 class="h1"><?= CHtml::encode($category->getTitle()); ?></h1>
            </div>
            <?php $this->widget(
                'zii.widgets.CListView', [
                    'dataProvider' => $dataProvider,
                    'itemView' => '//store/product/_item',
                    'template' => '
                        <div class="catalog-controls">
                            <div class="catalog-controls__col">{sorter}</div>
                        </div>
                        {items}
                        {pager}
                    ',
                    'summaryText' => '',
                    'enableHistory' => true,
                    'cssFile' => false,
                    'itemsCssClass' => 'catalog__items',
                    'sortableAttributes' => [
                        'sku',
                        'name',
                        'price',
                        'update_time'
                    ],
                    'sorterHeader' => '<div class="sorter__description">' . Yii::t("StoreModule.store", "Sort:") . '</div>',
                    'htmlOptions' => [
                        'class' => 'catalog'
                    ],
                    'pagerCssClass' => 'catalog__pagination',
                    'pager' => [
                        'header' => '',
                        'prevPageLabel' => '<i class="fa fa-long-arrow-left"></i>',
                        'nextPageLabel' => '<i class="fa fa-long-arrow-right"></i>',
                        'firstPageLabel' => false,
                        'lastPageLabel' => false,
                        'htmlOptions' => [
                            'class' => 'pagination'
                        ]
                    ],
                ]
            ); ?>
            <div class="description">
                <?= $category->getDescription(); ?>
            </div>
        </div>
    </div>
</div>
