<?php
/**
 * Entry point script:
 *
 * @category YupeScript
 * @package  YupeCMS
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/

/**
 * @link    http://www.yiiframework.ru/doc/guide/ru/basics.entry
 */

if (!ini_get('date.timezone')) {
    date_default_timezone_set('Europe/Moscow');
}

// Setting internal encoding to UTF-8.
if (!ini_get('mbstring.internal_encoding')) {
    @ini_set("mbstring.internal_encoding", 'UTF-8');
    mb_internal_encoding('UTF-8');
}

// Comment next two lines on production
define('YII_DEBUG', true);
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

require __DIR__ . '/../vendor/yiisoft/yii/framework/yii.php';

$base = require __DIR__ . '/../protected/config/main.php';
//$base = require __DIR__ . '/../protected/config/test.php';


//$base['components']['log']['routes']['trace']['enabled'] = true;
//$base['components']['log']['routes']['error']['enabled'] = true;
//$base['components']['log']['routes']['file']['enabled'] = true;
//$base['components']['log']['routes']['web']['enabled'] = true;
//$base['components']['log']['routes']['profile']['enabled'] = true;
//$base['components']['log']['routes']['summary']['enabled'] = true;
//$base['components']['urlManager']['showScriptName'] = true;
////$base['components']['db']['enableParamLogging'] = true;
////$base['components']['db']['enableProfiling'] = true;
////$base['components']['widgetFactory']['widgets']['COutputCache']['duration'] = -1;
//if(Yii::getLogger()->autoDump)
//    $base['components']['db']['enableProfiling'] = false;

$confManager = new yupe\components\ConfigManager();
$confManager->sentEnv(\yupe\components\ConfigManager::ENV_WEB);

require __DIR__ . '/../vendor/autoload.php';

Yii::createWebApplication($confManager->merge($base))->run();
